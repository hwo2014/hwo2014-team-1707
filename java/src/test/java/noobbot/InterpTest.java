package noobbot;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Created by rukshan on 4/19/14.
 */
public class InterpTest extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public InterpTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( InterpTest.class );
    }

    public void testSlipInt()
    {
        System.out.println(System.getProperty("user.dir"));
        DataProcessor dp = DataProcessor.getInstance();
        double result = dp.getSpeedForAngleAndRadius(2,45) ; //5.9
        System.out.print(" 5.9 -> " + result);
    }

    public void testSpeedInTicks()
    {
        //3.9214633921659896
        //5.873767182370262
        TrackKnowledge tk = TrackKnowledge.getInstance();
        tk.setDragConstant(0.09468230773728731);
        tk.setMass(4.275955305580774);

        System.out.println(tk.speedInGivenAmountOfTicks(1,3.420867568567047   ,1,1));
        System.out.println(tk.getThrottle(6.69914423015638 , 7.028639287637698,1,1));


        tk.setDragConstant(0.13979968112459265);
        tk.setMass(6.1112612882108985 );
        System.out.println(tk.speedInGivenAmountOfTicks(1,5.6922216272221124  ,0.6560635426057227,1));
        //7.5525476119675865  //7.637761884391029


    }

    public void testDegreeRadians(){
        CarState cs = new CarState();
        Assert.assertEquals( Math.PI, cs.toRadians(180));
        Assert.assertEquals( Math.PI/2, cs.toRadians(90));
    }
}
