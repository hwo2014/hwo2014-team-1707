package noobbot;

import com.google.gson.Gson;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


public class CarPositionsTest extends TestCase {
	
	/**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public CarPositionsTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( CarPositionsTest.class );
    }
    
	public void testParse(){
		
		Gson gson = new Gson();
        String json = "{\"msgType\": \"carPositions\", \"data\": ["
        +"{\"id\":{\"name\": \"Schumacher\",  \"color\": \"red\"}, \"angle\": 0.0, \"piecePosition\":{"
        + "\"pieceIndex\": 0,\"inPieceDistance\": 0.0, \"lane\": {\"startLaneIndex\": 0,"
        + "\"endLaneIndex\": 0},\"lap\": 0}}, {\"id\": {\"name\": \"Rosberg\", \"color\": \"blue\""
        + "}, \"angle\": 45.0, \"piecePosition\": { \"pieceIndex\": 3,  \"inPieceDistance\": 20.0,"
       + "\"lane\": { \"startLaneIndex\": 1,  \"endLaneIndex\": 2 }, \"lap\": 3"
        + "}}], \"gameId\": \"OIUHGERJWEOI\", \"gameTick\": 0}";
        
     final MsgWrapper msgFromServer = gson.fromJson(json, MsgWrapper.class);
        
        CarPositions cp = CarPositions.parse(json, "Rosberg");  
        
        assertEquals(3, cp.getMyCarPosition().getTrackIndex());
        assertEquals(20.0, cp.getMyCarPosition().getInPieceDistance());
        assertEquals(3, cp.getMyCarPosition().getLap());
        assertEquals(45.0, cp.getMyCarPosition().getSlipAngle());
        assertEquals(1, cp.getMyCarPosition().getStartLaneIndex());
        assertEquals(2, cp.getMyCarPosition().getEndLaneIndex());
        assertNotNull(cp);
        
		
	}

}
