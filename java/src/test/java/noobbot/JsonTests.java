package noobbot;

import java.io.*;
import java.util.ArrayList;
//import java.util.function.Consumer;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


//Just to see how the json library works
public class JsonTests extends TestCase {

	/**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public JsonTests( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( JsonTests.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testGameInitJson()
    {
    	 Gson gson = new Gson();
        String json = "{\"msgType\": \"gameInit\", \"data\": {\"race\": { "
                     + "\"trackName\": {"
                     + "\"id\": \"indianapolis\","
                     + "\"name\": \"Indianapolis\", "
                     + "\"pieces\": ["
    + "{ \"length\": 100.0},{\"length\": 100.0,\"switch\": true},{\"radius\": 200,\"angle\": 22.5}],"
    + "\"lanes\": [{\"distanceFromCenter\": -20,\"index\": 0},{\"distanceFromCenter\": 0,\"index\": 1},"
    + "{\"distanceFromCenter\": 20,\"index\": 2}],"
    +"\"startingPoint\": {\"position\": {\"x\": -340.0,\"y\": -96.0},\"angle\": 90.0 }},"
    + "\"cars\": [{\"id\": {\"name\": \"Schumacher\", \"color\": \"red\" }, \"dimensions\": {"
    + "\"length\": 40.0, \"width\": 20.0, \"guideFlagPosition\": 10.0  }},"
    + "{\"id\": {\"name\": \"Rosberg\", \"color\": \"blue\" }, \"dimensions\": { \"length\": 40.0,"
    + "\"width\": 20.0, \"guideFlagPosition\": 10.0 }}], \"raceSession\": { \"laps\": 3,"
    + "\"maxLapTimeMs\": 30000,"
    + " \"quickRace\": true  }}}}" ;
        
        final MsgWrapper msgFromServer = gson.fromJson(json, MsgWrapper.class);
       // Object data = msgFromServer.data.
        LinkedTreeMap tm = (LinkedTreeMap<String, Object>)msgFromServer.data;
        LinkedTreeMap racemap = (LinkedTreeMap<String, Object>)tm.get("race");
        LinkedTreeMap trackm = (LinkedTreeMap<String, Object>)racemap.get("trackName");
        
        ArrayList<Object> pal = (ArrayList<Object>) trackm.get("pieces");
        
        for(Object o : pal){
        	LinkedTreeMap m = (LinkedTreeMap<String, String>)o;
        	
        	
        	if(m.containsKey("length"))
        	{
        		System.out.println(m.get("length"));
        	}
        	else
        	{
        		System.out.println(m.get("radius"));
        	}
        	
        }
        
        System.out.println(msgFromServer.msgType);

    }

    public void testMakeSlip(){

        final Gson gson = new Gson();
        File[] files = getDataFiles();

        StringBuilder builder = new StringBuilder();

        ArrayList<Data> resultData = new ArrayList<>();

        for(File f : files){
            ArrayList<Data> data = new ArrayList<>();
            try{
                BufferedReader br = new BufferedReader(new FileReader(f.getPath()));
                String line;




                while ((line = br.readLine()) != null) {

                    // process the line.
                    Data d = gson.fromJson(line,Data.class);
                    data.add(d);

                }
                br.close();


            }
            catch (Exception ex){

            }

            boolean crashed = false;
            double crashedRadius = 0 ;
            double crashedAngle = 0 ;

            for(int i = data.size() -1 ; i >= 0 ; i--){
                Data d = data.get(i);
                double absSlip = Math.abs(d.slipAngle);

                if(d.speed == 0){
                    crashed = true;
                    crashedAngle = d.angle;
                    crashedRadius = d.radius;
                }
                else{
                    if(d.angle != crashedAngle && d.radius != crashedRadius)
                        crashed = false;
                }

                if( absSlip >= 1 && d.mass > 0  && d.isBend  && d.speed > 0 && d.k > 0 && d.force > 0  && !crashed  ){
                    // builder.append(line + "\n");
                    resultData.add(d);

                }
            }
        }

        /*ArrayList<Data> filteredData = new ArrayList<>();

        for(Data r: resultData){

            if(containsSlipAngle(r, filteredData))
                continue;
            //get max speed for slip
            double f = getMaxForceForSlip(r.slipAngle, r.force, r.angle, r.radius, r.mass, r.k, resultData);

            r.force = f;

            filteredData.add(r);
        } */

        ArrayList<Data> highestSlipData = new ArrayList<>();

        for(Data r: resultData){

            if(containsHighestSlip(r, highestSlipData))
                continue;
            //get max speed for slip
            Data hd = getMaxSlipData(r, resultData);
 

            highestSlipData.add(hd);
        }


        for(Data r: highestSlipData){
            builder.append(gson.toJson(r, Data.class) + "\n");
        }



        try{
            PrintWriter writer = new PrintWriter(Main.SLIP_DATA_FILE, "UTF-8");
            writer.print(builder.toString());
            writer.close();
        }
        catch (Exception ex){

        }
    }

    private double getMaxForceForSlip(double slip, double force , double angle, double radius, double mass, double k, ArrayList<Data> data){
        angle = Math.abs(angle);
        slip = Math.abs(angle);
        for(Data d : data){
            if(d.mass == mass && k == d.k && Math.abs(d.angle) == angle && radius == d.radius && Math.abs(d.slipAngle) <=  slip){
                if(force < d.force)
                    force = d.force;
            }
        }

        return  force;
    }

    private Data getMaxSlipData(Data s, ArrayList<Data> data){

        for(Data d : data){
            if(d.mass == s.mass && s.k == d.k){
                if(s.force < d.force)
                    s = d;
            }
        }

        return  s;
    }

    private boolean containsSlipAngle(Data data, ArrayList<Data> currentDataList){
        for(Data d : currentDataList){
            if(d.mass == data.mass && d.k == data.k && Math.abs(d.angle) == Math.abs(data.angle) && d.radius == data.radius && Math.abs(d.slipAngle) <= Math.abs(data.slipAngle)){
                return true;
            }
        }

        return false;
    }

    private boolean containsHighestSlip(Data data, ArrayList<Data> currentDataList){
        for(Data d : currentDataList){
            if(d.mass == data.mass  && d.k == data.k){
                return true;
            }
        }

        return false;
    }

    private File[] getDataFiles(){
        File dir = new File("./massdata");
        File[] files = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".data");
            }
        });

        return  files;
    }


}
