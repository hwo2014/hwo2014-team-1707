package noobbot;

import com.google.gson.Gson;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TrackHelperTests extends TestCase {
	/**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public TrackHelperTests( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( TrackHelperTests.class );
    }
    
	public void testParseWithTrackName(){
		
		Gson gson = new Gson();

        
        String gameInitJson = "{\"msgType\": \"gameInit\", \"data\": {\"race\": { "
                + "\"trackName\": {"
                + "\"id\": \"indianapolis\","
                + "\"name\": \"Indianapolis\", "
                + "\"pieces\": ["
+ "{ \"length\": 100.0},{\"length\": 100.0,\"switch\": true},{\"radius\": 200,\"angle\": 22.5},{\"length\": 30.0}],"
+ "\"lanes\": [{\"distanceFromCenter\": -20,\"index\": 0},{\"distanceFromCenter\": 0,\"index\": 1},"
+ "{\"distanceFromCenter\": 20,\"index\": 2}],"
+"\"startingPoint\": {\"position\": {\"x\": -340.0,\"y\": -96.0},\"angle\": 90.0 }},"
+ "\"cars\": [{\"id\": {\"name\": \"Schumacher\", \"color\": \"red\" }, \"dimensions\": {"
+ "\"length\": 40.0, \"width\": 20.0, \"guideFlagPosition\": 10.0  }},"
+ "{\"id\": {\"name\": \"Rosberg\", \"color\": \"blue\" }, \"dimensions\": { \"length\": 40.0,"
+ "\"width\": 20.0, \"guideFlagPosition\": 10.0 }}], \"raceSession\": { \"laps\": 3,"
+ "\"maxLapTimeMs\": 30000,"
+ " \"quickRace\": true  }}}}" ;
        
     final MsgWrapper msgFromServer = gson.fromJson(gameInitJson, MsgWrapper.class);
        
        TrackHelper th = TrackHelper.getInstance();
        th.LoadTrack(msgFromServer);
        
        assertEquals(185.0, th.getDistanceToNextTrackType(0, 15,0));
        assertEquals(200.0,  th.getDistanceToNextTrackType(0, 0,1));
        assertEquals(91.0,  th.getDistanceToNextTrackType(1, 9,2));
        assertEquals(2.0,  th.getDistanceToNextTrackType(1, 98,1));
        assertEquals(206.0,  th.getDistanceToNextTrackType(3, 24,1));
        assertEquals(200.0,  th.getDistanceToNextTrackType(3, 30,2));
        assertEquals(230.0,  th.getDistanceToNextTrackType(3, 0,0));
        System.out.println(th.getDistanceToNextTrackType(2, 0,0));


        assertEquals(3, th.getLanes().size());
        assertEquals(20.0, th.getLane(2).getDistanceFromCenter());



        
		
	}

    public void testParseWithTrack(){

        Gson gson = new Gson();


        String gameInitJson = "{\"msgType\": \"gameInit\", \"data\": {\"race\": { "
                + "\"track\": {"
                + "\"id\": \"indianapolis\","
                + "\"name\": \"Indianapolis\", "
                + "\"pieces\": ["
                + "{ \"length\": 100.0},{\"length\": 100.0,\"switch\": true},{\"radius\": 200,\"angle\": 22.5},{\"length\": 30.0}],"
                + "\"lanes\": [{\"distanceFromCenter\": -20,\"index\": 0},{\"distanceFromCenter\": 0,\"index\": 1},"
                + "{\"distanceFromCenter\": 20,\"index\": 2}],"
                +"\"startingPoint\": {\"position\": {\"x\": -340.0,\"y\": -96.0},\"angle\": 90.0 }},"
                + "\"cars\": [{\"id\": {\"name\": \"Schumacher\", \"color\": \"red\" }, \"dimensions\": {"
                + "\"length\": 40.0, \"width\": 20.0, \"guideFlagPosition\": 10.0  }},"
                + "{\"id\": {\"name\": \"Rosberg\", \"color\": \"blue\" }, \"dimensions\": { \"length\": 40.0,"
                + "\"width\": 20.0, \"guideFlagPosition\": 10.0 }}], \"raceSession\": { \"laps\": 3,"
                + "\"maxLapTimeMs\": 30000,"
                + " \"quickRace\": true  }}}}" ;

        final MsgWrapper msgFromServer = gson.fromJson(gameInitJson, MsgWrapper.class);

        TrackHelper th = TrackHelper.getInstance();
        th.LoadTrack(msgFromServer);

        assertEquals(185.0,  th.getDistanceToNextTrackType(0, 15,0));
        assertEquals(200.0,  th.getDistanceToNextTrackType(0, 0,2));
        assertEquals(91.0,  th.getDistanceToNextTrackType(1, 9,1));
        assertEquals(2.0,  th.getDistanceToNextTrackType(1, 98,2));
        assertEquals(206.0,  th.getDistanceToNextTrackType(3, 24,1));
        assertEquals(200.0,  th.getDistanceToNextTrackType(3, 30,0));
        assertEquals(230.0,  th.getDistanceToNextTrackType(3, 0,2));








    }


}
