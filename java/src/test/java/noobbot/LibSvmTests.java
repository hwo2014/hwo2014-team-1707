package noobbot;

/**
 * Created by rukshan on 4/20/14.
 */

import com.google.gson.Gson;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import libsvm.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

public class LibSvmTests extends TestCase{


    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public LibSvmTests( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( LibSvmTests.class );
    }


    double[][] train = new double[1000][];
    double[][] test = new double[10][];

    public void mtestLibSvm()
    {


        for (int i = 0; i < train.length; i++){
            if (i+1 > (train.length/2)){        // 50% positive
                double[] vals = {0.1,0,i+i};
                train[i] = vals;
            } else {
                double[] vals = {0.3,0,i-i-i-2}; // 50% negative
                train[i] = vals;
            }
        }

        svm_model model = svmTrain();

        double[] t = {0.3,1,20000.3};
        evaluate(t,svmTrain());


    }


    private svm_model svmTrain() {
        svm_problem prob = new svm_problem();

        int dataCount = train.length;
        prob.y = new double[dataCount];
        prob.l = dataCount;
        prob.x = new svm_node[dataCount][];

        for (int i = 0; i < dataCount; i++){
            double[] features = train[i];
            prob.x[i] = new svm_node[features.length-1];
            for (int j = 1; j < features.length; j++){
                svm_node node = new svm_node();
                node.index = j;
                node.value = features[j];
                prob.x[i][j-1] = node;
            }
            prob.y[i] = features[0];
        }

        svm_parameter param = new svm_parameter();
        param.probability = 1;
        param.gamma = 0.5;
        param.nu = 0.5;
        param.C = 1;
        param.svm_type = svm_parameter.NU_SVR;
        param.kernel_type = svm_parameter.NU_SVR;
        param.cache_size = 20000;
        param.eps = 0.001;

        svm_model model = svm.svm_train(prob, param);

        return model;
    }

    public double evaluate(double[] features, svm_model model)
    {
        svm_node[] nodes = new svm_node[features.length-1];
        for (int i = 1; i < features.length; i++)
        {
            svm_node node = new svm_node();
            node.index = i;
            node.value = features[i];

            nodes[i-1] = node;
        }

        int totalClasses = 2;
        int[] labels = new int[totalClasses];
        svm.svm_get_labels(model,labels);

        double[] prob_estimates = new double[totalClasses];
        double v = svm.svm_predict_probability(model, nodes, prob_estimates);

        for (int i = 0; i < totalClasses; i++){
            System.out.print("(" + labels[i] + ":" + prob_estimates[i] + ")");
        }
        System.out.println("(Actual:" + features[0] + " Prediction:" + v + ")");

        return v;
    }

    public void testSlipSvm() throws Exception
    {
        System.out.println(System.getProperty("user.dir"));
        DataProcessor dp = DataProcessor.getInstance();


        final Gson gson = new Gson();
        ArrayList<Data> data = new ArrayList<>();
        try{
            BufferedReader br = new BufferedReader(new FileReader("./slip.txt"));
            String line;




            while ((line = br.readLine()) != null) {

                // process the line.
                Data d = gson.fromJson(line,Data.class);
                data.add(d);

            }
            br.close();


        }
        catch (Exception ex){

        }


        double delta = 0.1;
        int correct = 0 ;

        for(Data d : data){
            double p = dp.getForceUsingSvm(0,d.mass, 0) ;

            if( ( d.force - delta < p  &&  p < d.force + delta  ))
                correct++;
        }

        System.out.println("total correct: " + correct+" correctness: " + (double) correct / data.size());


      /*  System.out.println(dp.getForceUsingSvm(0,4.6427694847596355,0));//"force":0.47}
        System.out.println(dp.getForceUsingSvm(0,4.997628289008405,0));//"force":0.45}
        System.out.println(dp.getForceUsingSvm(0,5.826888153865878,0));//"force":0.5}


System.out.println(dp.getForceUsingSvm(0,5.867009378754987,0));//"force":0.31617740511743203}
        System.out.println(dp.getForceUsingSvm(0,4.363849216425601,0));//"force":0.3498135741198178}
        System.out.println(dp.getForceUsingSvm(0,4.966583281490551,0));//,"force":0.3220475430393266}
        System.out.println(dp.getForceUsingSvm(0,5.196576574130152,0));//"force":0.4153552393128188}
        System.out.println(dp.getForceUsingSvm(0,5.2055362727871355,0));//"force":0.4446583876395009}
        System.out.println(dp.getForceUsingSvm(0,5.57958299535179,0));//,"force":0.40386859887035886}
        System.out.println(dp.getForceUsingSvm(0,4.722042761199612,0));//,"force":0.4303876493600048}
        System.out.println(dp.getForceUsingSvm(0,0.08280800072889014,0));//"force":0.5185065025870472}
        System.out.println(dp.getForceUsingSvm(0,4.151887934963408,0));//,"force":0.5282000439460071}
        System.out.println(dp.getForceUsingSvm(0,5.157476531045907,0));//"force":0.4700475240270324}
        System.out.println(dp.getForceUsingSvm(0,4.893352864434988,0));//"force":0.34954185460130305}
        System.out.println(dp.getForceUsingSvm(0,4.660396117715003,0));//"force":0.4957150040646829} */

    //    {"throttle":1.0,"speed":21.09399007545855,"acceleration":0.0390729915991983,"slipAngle":3.7281802881186477,"isBend":true,"radius":210.0,"angle":-22.5,"tick":375.0,"mass":5.888628405719483}
        //System.out.print(" 5.9 -> " + result);
    }

    public void testTrainSlipModelAndSave() throws  Exception{
        SlipSvm slipSvm = new SlipSvm();


        DataProcessor dp = DataProcessor.getInstance();
        ArrayList<String> dataStrings = Data.getSlip40to50();
        ArrayList<Data> data =  dp.getSlipDataFromJson(dataStrings);
        slipSvm.train(data);
        slipSvm.saveModel();

    }
}
