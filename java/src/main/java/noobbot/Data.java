package noobbot;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

/**
 * Created by rukshan on 4/19/14.
 */
public class Data {
    public double throttle;
    public double speed;
    public double acceleration;
    public double slipAngle;
    public boolean isBend ;
    public double radius;
    public double angle;
    public double tick;
    public double mass;
    public double k ;
    public double force;


    public static ArrayList<String> getSlip40to50(){

        ArrayList<String> result = new ArrayList<>();
        try{
            BufferedReader br = new BufferedReader(new FileReader(Main.SLIP_DATA_FILE));
            String line;

            while ((line = br.readLine()) != null) {
                // process the line.
                result.add(line);
            }
            br.close();
        }
        catch (Exception ex){

        }

        return  result;
    }
}


