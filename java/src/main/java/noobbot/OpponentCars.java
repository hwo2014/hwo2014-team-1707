package noobbot;

import java.util.ArrayList;
import java.util.HashMap;

public class OpponentCars {
	
	private HashMap<String, Double> carSpeeds = new HashMap<>(); 
	private HashMap<String,CarPosition>  otherCarPositions = new HashMap<>();
	
	private static OpponentCars instance;
	
	private OpponentCars(){
		
	}
	
	public static OpponentCars getInstance(){
		if(instance == null)
			instance = new OpponentCars();
		
		return instance;
	}
	
 
	
	public void Update(CarPositions carPositions){

		TrackHelper track = TrackHelper.getInstance();
		HashMap<String,CarPosition> newPositions = carPositions.getOtherCarPositions();
		
	    for(String name : newPositions.keySet()){
	    	
	    	if(otherCarPositions.containsKey(name)){
	    		
	    		CarPosition oldPosition = otherCarPositions.get(name);
	    		CarPosition newPosition = newPositions.get(name);
	    		double speed = track.getDistanceTravelled(oldPosition.getTrackIndex(), newPosition.getTrackIndex(), oldPosition.getInPieceDistance(), newPosition.getInPieceDistance(), newPosition.getEndLaneIndex());
	    		carSpeeds.remove(name);
	    		
	    		if(!carSpeeds.containsKey(name))
	    			carSpeeds.put(name, speed);
	    		
	    	}
	    	else{
	    		//it is a new car
	    		carSpeeds.put(name, 0.0);
	    	}
	    }

		otherCarPositions = newPositions;
	}
	
	public int bestLaneDirFromOnePieceToAnotherPiece(int bestDirForBends, Lane currentLane, double mySpeed, TrackPiece closestSwitch, TrackPiece nextSwitchPiece, int lap, double mass){
		
		// When checking lanes for emptiness consider the speed of the cars to see if they would clear the next switch 
		// before we get close to them
		// 1. if there is no one in the current lane it is the best
		// 2. if there is another neighbor lane which is free , return that
		// 3. At this point all lanes are occupied.
		// 4. which lane has the highest slowest car (get the slowest from each lane and pick the highest from them)

        TrackHelper track = TrackHelper.getInstance();

/*        int bendLaneIndex = currentLane.getIndex();

        if( (bestDirForBends > 0  &&   numberOfLanes - 1  <= currentLane.getIndex())
                || (bestDirForBends < 0 && currentLane.getIndex() <= 0)){
            // current lane is the best
        }
        else {
           bendLaneIndex = bestDirForBends > 0 ? currentLane.getIndex() + 1 : currentLane.getIndex() -1;
        }*/

        //calculate potential speed between the switches
        double potentialSpeed = getPotentialAverageSpeedBetweenTrackPieces(closestSwitch,nextSwitchPiece, currentLane, lap, mass);

        Lane  bendLane =  track.getLaneInDir(bestDirForBends,currentLane.getIndex());

        if(bendLane == null)
            bendLane = currentLane;



        if(isLaneClearTillNextSwitch(closestSwitch, nextSwitchPiece, bendLane, potentialSpeed)){
           Main.LogLn(Main.tick + ": BEND LANE IS CLEAR");
           return (bendLane.getIndex() == currentLane.getIndex() ? 0 : (bendLane.getIndex()> currentLane.getIndex() ? 1 : -1));
        }

        Main.LogLn(Main.tick + ": BEST BEND LANE IS NOT CLEAR");
        //best lane in terms of bends is not clear
        boolean bendLaneIsCurrent = true;
        //see if the current lane is clear there is no need to change
        if(bendLane.getIndex() != currentLane.getIndex()){
            bendLaneIsCurrent = false;
            if(isLaneClearTillNextSwitch(closestSwitch, nextSwitchPiece, currentLane, potentialSpeed)){
                return 0;
            }
        }

        ArrayList<Integer> possibleLanes = new ArrayList<>();
        possibleLanes.add(currentLane.getIndex());

        Main.LogLn(Main.tick + ": CHECK THE NEIGHBOR Lanes");
        //check the neighbors
        if(!bendLaneIsCurrent){
            Main.LogLn(Main.tick+ ": Current is not the bend lane. Check other lane");
            //current lane is not the bend lane and both of them are not clear
            //check the other lane if there is one
            Lane otherLane = track.getLaneInDir(bestDirForBends * (-1), currentLane.getIndex()); 
            if(otherLane != null){
                Main.LogLn(Main.tick+": CHECK THE NEIGHBOR Lanes");
                if(isLaneClearTillNextSwitch(closestSwitch, nextSwitchPiece, otherLane, potentialSpeed)){
                    return bestDirForBends * (-1);
                }

                if(!possibleLanes.contains(otherLane.getIndex()))
                    possibleLanes.add(otherLane.getIndex());

            }
        }
        else{
            //current is the bend lane
            Main.LogLn(Main.tick+ ": Current is the bend lane. Check other two lanes");

            int d = 1;

            if(currentLane.getIndex() != 0)
                d = -1;
            Lane otherLane = track.getLaneInDir(d, currentLane.getIndex());

            if(otherLane != null){
                Main.LogLn(Main.tick+ ": Check lane in direction : " + d);
                if(isLaneClearTillNextSwitch(closestSwitch, nextSwitchPiece, otherLane, potentialSpeed)){
                    return d;
                }

                if(!possibleLanes.contains(otherLane.getIndex()))
                    possibleLanes.add(otherLane.getIndex());
            }
        }

        //No luck so far. All lanes are occupied
        Main.LogLn(Main.tick+": All lanes occupied.");


        int fastestLane = getLeastSlowLane(possibleLanes,closestSwitch, nextSwitchPiece, currentLane);
        int currentLaneIndex = currentLane.getIndex();

        if(fastestLane == currentLaneIndex)
            return 0;

		return fastestLane > currentLaneIndex ? 1 : -1;
		
	}
	
	public boolean isLaneClearTillNextSwitch(TrackPiece fromSwitchPiece, TrackPiece nextSwitchPiece,  Lane lane, double mySpeed){
		
		//get the closest car to the fromSwitchPiece
		TrackHelper track = TrackHelper.getInstance();
		
		if(nextSwitchPiece == null)
		{
			nextSwitchPiece = track.getLastTrackPiece();
		}
		
		double distanceBetweenSwitches = track.getDistance(fromSwitchPiece, nextSwitchPiece, 0.0, lane);
		double myCarTimeToNextSwitch = getTime(distanceBetweenSwitches, mySpeed);

		for(String name : this.otherCarPositions.keySet()){
			CarPosition car = this.otherCarPositions.get(name);
			
			if(car.getEndLaneIndex() != lane.getIndex())
				continue;
			
		    double distanceFromSwitch =track.getDistance(fromSwitchPiece, track.getTrackPiece(car.getTrackIndex()), 0.0, lane);
		    
		    if(distanceFromSwitch > distanceBetweenSwitches)
		    	continue;
		    
		    if(!this.carSpeeds.containsKey(name))
		    	continue;
		    
		    double timeToNextSwitch = getTime(distanceBetweenSwitches - distanceFromSwitch , this.carSpeeds.get(name));
		    
		    if(timeToNextSwitch > myCarTimeToNextSwitch)
		    	return false;
		}
		
		return true;
	}
	
	private double getTime(double distance ,  double speed){
		if(speed == 0.0)
			return Double.MAX_VALUE;
		
		return distance / speed;
	}

    private double getPotentialAverageSpeedBetweenTrackPieces(TrackPiece startPiece, TrackPiece endPiece, Lane myLane, int currentLap, double mass){


        if(endPiece == null)
            return TrackKnowledge.DefaultMaxSpeed;

        int n = 0 ;
        double total = 0.0;

        TrackPiece current = startPiece;

        while(current.getIndex() == endPiece.getIndex()){
            n++;
            if(current.getType() == TrackPiece.PieceType.STRAIGHT){
                total += TrackKnowledge.DefaultMaxSpeed;
            }
            else{
               try{
                    total +=  TrackKnowledge.getInstance().maxPossibleSpeed(current.getIndex(), current.getRadius(myLane));
               }
               catch (Exception ex){
                   ex.printStackTrace();
               }
            }

            current = TrackHelper.getInstance().getNextTrackPiece(current.getIndex(),currentLap);

            if(current == null)
                break;

        }

        if(n != 0)
            return total / n ;
        else
            return TrackKnowledge.DefaultMaxSpeed;


    }

    private int getLeastSlowLane(ArrayList<Integer> laneIndexes, TrackPiece fromSwitchPiece, TrackPiece nextSwitchPiece,  Lane lane){

        TrackHelper track = TrackHelper.getInstance();

        HashMap<Integer, Double> slowestLaneSpeed = new HashMap<>();

        for(String name : this.otherCarPositions.keySet()){
            CarPosition car = this.otherCarPositions.get(name);

            if(car == null)
                continue;

            if(!laneIndexes.contains(car.getEndLaneIndex())){
                //this car is in a lane we are not interested in
                continue;
            }

            double distanceFromSwitch =track.getDistance(fromSwitchPiece, track.getTrackPiece(car.getTrackIndex()), 0.0, lane);
            double distanceBetweenSwitches = track.getDistance(fromSwitchPiece, nextSwitchPiece, 0.0, lane);

            if(distanceFromSwitch > distanceBetweenSwitches)
                continue;

            if(!this.carSpeeds.containsKey(name))
                continue;

            int carLaneIndex = car.getEndLaneIndex();
            double carSpeed = this.carSpeeds.get(name);

            if(slowestLaneSpeed.containsKey(carLaneIndex)){
                if(carSpeed < slowestLaneSpeed.get(carLaneIndex))
                    slowestLaneSpeed.remove(carLaneIndex);
                    slowestLaneSpeed.put(carLaneIndex, carSpeed);
            }
            else{
                slowestLaneSpeed.put(carLaneIndex, carSpeed);
            }
        }

        if(slowestLaneSpeed.size() == 0 )
            return lane.getIndex();

        int fastestLane = -1 ;
        double fastesSpeed = 0 ;
        for(int i : slowestLaneSpeed.keySet()){
            double s = slowestLaneSpeed.get(i);

            if(s > fastesSpeed){
                fastesSpeed = s;
                fastestLane = i;
            }
        }

        if(fastestLane < 0)
            return lane.getIndex();

        return fastestLane;
    }

    public void removeFinishedCar(String name){
        if(carSpeeds.containsKey(name))
            carSpeeds.remove(name);

        if(otherCarPositions.containsKey(name))
            otherCarPositions.remove(name);
    }
	
}
