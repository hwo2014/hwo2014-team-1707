package noobbot;

import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by rukshan on 4/19/14.
 */
public class DataProcessor {

    private static DataProcessor instance;
    ArrayList<String> slipData ;
    Interpolation slipInterpolation ; // radius, angle -> speed
    SlipSvm slipSvm;
    ThrottleSvm throttleSvm;

    private DataProcessor(){}

    public static DataProcessor getInstance(){
        if(instance == null)
            instance = new DataProcessor();

        return instance;
    }

    public double getSpeedForAngleAndRadius(double radius, double angle){

        if(slipInterpolation == null){
            slipInterpolation = getSlipInterpolation();
        }

        return slipInterpolation.linearInterpolation(radius);
    }

    public double getForceUsingSvm(double radius, double mass, double k) throws Exception{

        if(slipSvm == null){
            slipSvm = new SlipSvm();
        }

        return slipSvm.predict(radius, mass, k);
    }


    public double getThrottleUsingSvm(double fromSpeed, double toSpeed) throws Exception{

        if(throttleSvm == null){
            throttleSvm = new ThrottleSvm();
        }

        return throttleSvm.predict(fromSpeed, toSpeed);
    }


    private Interpolation getSlipInterpolation(){
        ArrayList<String> dataStrings = Data.getSlip40to50();
        ArrayList<Data> data = getSlipDataFromJson(dataStrings);

        double[] radius = new double[data.size()];
      //  double[] angle = new double[data.size()];
        double[] speed = new double[data.size()];

        for(int i = 0 ; i < data.size() ;i++){
            Data d = data.get(i);
            radius[i] = d.radius;
    //        angle[i] =  Math.abs(d.angle);
            speed[i] = d.speed;
        }

        Interpolation interp = new Interpolation(radius,speed, true);
        return interp;
    }

    public ArrayList<Data> getSlipDataFromJson(ArrayList<String> data){
        Gson gson = new Gson();

        ArrayList<Data> r = new ArrayList<>();

        for(String j : data){
            Data d = gson.fromJson(j, Data.class);
            r.add(d);
        }

        return r;
    }

}
