package noobbot;

import java.util.ArrayList;

/**
 * Created by rukshan on 4/18/14.
 */
public class BendStats {

    static BendStats instance;
    double minRadius ;
    double maxRadius  ;
    double upperSlipAngle = 55;

    TrackHelper track;

    ArrayList<BendData> pastData = new ArrayList<>();

    private BendStats(){}


    public void init(TrackHelper track){
        this.track = track;

        for(TrackPiece p : track.pieces){
            if(p.getPieceType() == TrackPiece.PieceType.BEND){
                if(minRadius == 0) minRadius = p.getRadius();
                if(maxRadius ==0 ) maxRadius = p.getRadius();

                if(p.getRadius() > maxRadius)
                    maxRadius = p.getRadius();

                if(p.getRadius() < minRadius)
                    minRadius = p.getRadius();
            }
        }

        System.out.println("MAX RADIUS: " + maxRadius);
        System.out.println("MIN RADIUS: " + minRadius);
    }

    public void addBendData(BendData data){
    //	System.out.println("Bend data index" + data.trackIndex);
        pastData.add(data);
    }

    public double getBestThrottleForTrackPiece(int trackPieceIndex, int lap, int laneIndex){
    	
        TrackPiece tp = track.getTrackPiece(trackPieceIndex);
    //    System.out.println("Bend data ? " +  trackPieceIndex);
        ArrayList<BendData> history = getHistory(trackPieceIndex);
        
        if(history.size() > 0){
        //	System.out.println("has history");

        	//we have seen this bend before
        	double tempBadThrottle = 1;
        	double tempGoodThrottle = 0 ;
            int tempLap = 0 ;

        	for(BendData bd : history){
        		if(!bd.isSafe && bd.throttle < tempBadThrottle){
        			tempBadThrottle = bd.throttle;
                    tempLap = bd.lap;
        		}
        		
        		if(bd.isSafe && bd.throttle > tempGoodThrottle)
        		{
        			tempGoodThrottle = bd.throttle;
                    tempLap = bd.lap;
        		}
        	}
        	
        	double result = 0 ;
        	if(tempGoodThrottle > 0 && tempBadThrottle < 1){
        		result = tempBadThrottle - 0.2;
          //  	System.out.println("history downgrades: " + result);

        	}
        	else{

                if(tempLap != lap)
                    result = tempGoodThrottle + 0.05;
            	else
                    result = tempGoodThrottle ;

            //    System.out.println("history upgrades: " + result);

        	}
        	
        	if(result < 0 ) result = 0 ;
        	if(result > 1) result = 1;
        	
        	return result;
        }
        double radius = tp.getRadius(track.getLane(laneIndex));

        double throttle = (radius - 50 ) /(150 - 50) * 0.7 ;
        //System.out.println("RADIUS: " + radius +" THROTTLE: " + throttle);
        if(throttle > 1) throttle = 1;
        if(throttle < 0) throttle = 0.1;
        return throttle;
    }

    private static BendStats getInstance(){
        if(instance == null)
            instance = new BendStats();

        return instance;
    }
    
    private ArrayList<BendData> getHistory(int trackPieceIndex){
    	ArrayList<BendData> bends = new ArrayList<BendData>();
    	for(BendData bd : pastData){
    		if(bd.trackIndex == trackPieceIndex)
    			bends.add(bd);
    	}
    	return bends;
    	
    }
    
    public boolean hasHistory(int trackPieceIndex){
    	for(BendData bd : pastData){
    		if(bd.trackIndex == trackPieceIndex)
    			return true;
    	}
    	return false;
    }

}


class BendData{
    int trackIndex;
    int lap;
    double slipAngle;
    double throttle;
    double speed;
    int laneIndex;
    boolean isSafe;
    double acceleration;
}
