package noobbot;

import libsvm.svm_model;

import java.io.*;
import java.util.Objects;

/**
 * Created by rukshan on 4/20/14.
 */
public class SvmUtil {
    public static void saveModel(svm_model model, String fileName){
        try{
            FileOutputStream fout = new FileOutputStream(fileName);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(model);
            oos.close();
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public static svm_model loadModel(String fileName){

        FileInputStream fis = null;
        ObjectInputStream in = null;

        svm_model obj = null;

        try
        {
            fis = new FileInputStream(fileName);
            in = new ObjectInputStream(fis);
            obj = (svm_model)in.readObject();
            in.close();

        }
        catch(IOException ex)
        {
            ex.printStackTrace();
        }
        catch(ClassNotFoundException ex)
        {
            ex.printStackTrace();
        }

        return obj;
    }
}
