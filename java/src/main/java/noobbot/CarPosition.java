package noobbot;

public class CarPosition {
	
	private int pieceIndex;
	private double inPieceDistance;
	private double angle;
    private int lap;
    private int startLaneIndex;
    private int endLaneIndex;
    private double speed ; // Used only for other cars

	public int getTrackIndex() {
		return pieceIndex;
	}
	public void setPieceIndex(int pieceIndex) {
		this.pieceIndex = pieceIndex;
	}
	public double getInPieceDistance() {
		return inPieceDistance;
	}
	public void setInPieceDistance(double inPieceDistance) {
		this.inPieceDistance = inPieceDistance;
	}

    public double getSlipAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public int getLap() {
        return lap;
    }

    public void setLap(int lap) {
        this.lap = lap;
    }

    public int getStartLaneIndex() {
        return startLaneIndex;
    }

    public void setStartLaneIndex(int startLaneIndex) {
        this.startLaneIndex = startLaneIndex;
    }

    public int getEndLaneIndex() {
        return endLaneIndex;
    }

    public void setEndLaneIndex(int endLaneIndex) {
        this.endLaneIndex = endLaneIndex;
    }
	private double getSpeed() {
		return speed;
	}
	private void setSpeed(double speed) {
		this.speed = speed;
	}
}
