package noobbot;

import libsvm.*;

import java.util.ArrayList;

/**
 * Created by rukshan on 4/20/14.
 */
public class SlipSvm {

    svm_model model;

    public void train(ArrayList<Data> data){
        svm_problem prob = new svm_problem();
        double[][] train = getTrainingData(data);




        int dataCount = train.length;
        prob.y = new double[dataCount];
        prob.l = dataCount;
        prob.x = new svm_node[dataCount][];

        for (int i = 0; i < dataCount; i++){
            double[] features = train[i];
            prob.x[i] = new svm_node[features.length-1];
            for (int j = 1; j < features.length; j++){
                svm_node node = new svm_node();
                node.index = j;
                node.value = features[j];
                prob.x[i][j-1] = node;
            }
            prob.y[i] = features[0];
        }

        svm_parameter param = new svm_parameter();
        param.probability = 1 ;
        param.gamma = 0.000000000001;
        param.nu = 0.5;
        param.C = 0.001;
        param.svm_type = svm_parameter.EPSILON_SVR; // epsilon svr
        param.kernel_type = svm_parameter.RBF;  //c svc
        param.cache_size = 20000;
        param.eps = 0.00001;

        svm_model model = svm.svm_train(prob, param);



        this.model = model;
    }



    public void saveModel() throws Exception{
        if(model != null)
            SvmUtil.saveModel(model,Main.SLIP_SVM_FILE);
        else
            throw new Exception("Model is null");
    }



    public double predict(double radius, double mass, double k) throws  Exception{

        if(model == null)
            model = SvmUtil.loadModel(Main.SLIP_SVM_FILE);

        if(model == null)
            throw new Exception("Model not loaded") ;

        double[] features = {0 , mass} ;
        double value = (evaluate(features, model));

        return value;
    }

    public double evaluate(double[] features, svm_model model)
    {
        svm_node[] nodes = new svm_node[features.length-1];
        for (int i = 1; i < features.length; i++)
        {
            svm_node node = new svm_node();
            node.index = i;
            node.value = features[i];

            nodes[i-1] = node;
        }

        int totalClasses = 2;
        int[] labels = new int[totalClasses];
        svm.svm_get_labels(model,labels);

        double[] prob_estimates = new double[totalClasses];
        double v = svm.svm_predict_probability(model, nodes, prob_estimates);

  //      for (int i = 0; i < totalClasses; i++){
   //         System.out.print("(" + labels[i] + ":" + prob_estimates[i] + ")");
   //     }
   //     System.out.println("(Actual:" + features[0] + " Prediction:" + v + ")");

        return v;
    }


    // speed, radius, agnle ,slip
    private double[][] getTrainingData(ArrayList<Data> data){
        double[][] train = new double[data.size()][];
        int i = 0 ;
        for(Data d : data){
            double[] t = {d.force, d.mass };
            train[i] = t;
            i++;
        }

    /*   for (int i = 0; i < train.length; i++){
            if (i+1 > (train.length/2)){        // 50% positive
                double[] vals = {0.1,110,i+i};
                train[i] = vals;
            } else {
                double[] vals = {0.3,0,i-i-i-2}; // 50% negative
                train[i] = vals;
            }
        }*/

        return train;
    }
}
