package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.UUID;


import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

public class Main {

    public static String SLIP_DATA_FILE = "slip.txt" ;
    public static String SLIP_SVM_FILE = "slipsvm.model" ;

    public static double HIGHSPEED_SLIP = 45;
    public static double STRAIGHT_MAX_SLIP = 60;

    static  boolean debug = false ;
    static String trackName = "" ;

	String myCarName = "";
	public static double tick = 0;
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        String joinOrCreate = "";
        if(args.length >4)
            joinOrCreate = args[4];


        if(args.length >5)
            trackName = args[5];



        String count = "";
        if(args.length >6)
            count = args[6];

        String password = "";
        if(args.length >7)
            password = args[7];


        System.out.println("Requested trackName name: " + trackName);

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        if(trackName.isEmpty())
            new Main(reader, writer, new Join(botName, botKey));
        else
        {
            boolean create = false ;
            if(joinOrCreate.trim() .equals( "create") ){
                System.out.println("CREATE RACE");
                create = true;
            }

            int carCount = 1;

            if(!count.isEmpty())
                carCount = Integer.parseInt(count);

            new Main(reader, writer, new CreateRace(botName, botKey,trackName, create, carCount, password));
        }
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final SendMsg join) throws IOException {
        this.writer = writer;
        String line = null;
        
        TrackHelper track =  TrackHelper.getInstance();
      //  BendStats bendStats = BendStats.getInstance();
        CarState carState = new CarState();
        ArrayList<Data> data = new ArrayList<>();
        send(join);
        double v1 = 0 ;
        double v2 = 0 ;
        double v3 = 0 ;
        int baseMCalc = 0 ;

        boolean massCalculated = false ;
        /* temp variables /
        boolean crashedAtLeastOne = false;
        int tempLapCount = 0 ;

        end of temp */


        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
            	 
            	try {
            		CarPositions currentPositions = CarPositions.parse(line, myCarName);
            		OpponentCars.getInstance().Update(currentPositions);
                    tick = currentPositions.getGameTick();

                    double throttle = carState.getSuggestedThrottleV2(currentPositions, track);
                    if(tick > 2 + baseMCalc || massCalculated){
              

                	    if(carState.shouldUseTurbo(currentPositions, carState.getLastThrottle())){
                		    //send turbo message
                		    send(new TurboMessage());
                	    }
                	    else{

                            int switchDir = 0 ;

                            try{
                                 switchDir = carState.getSuggestedSwitch(currentPositions, track);
                            }
                            catch (Exception ex){

                            }

                    	    if(switchDir != 0){
                    	    	SwitchMessage switchMsg = new SwitchMessage(switchDir);
                	        	send(switchMsg);
                        	}
                        	else{
                        		send(new Throttle(throttle));
                    	    }
                	    }
                    }
                    else{

                        throttle = 1;

                        if(carState.getLastSpeed() == 0.0)
                            baseMCalc++;
                        //k = ( v(1) - ( v(2) - v(1) ) ) / v(1)2 * h;
                        //calculate mass
                        switch ((int)tick - baseMCalc){
                            case 0:

                                    v1 = carState.getLastSpeed();
                                    System.out.println("v1 = " + v1);
                                break;
                            case 1:
                                    v2 = carState.getLastSpeed();
                                    System.out.println("v2 = " + v2);

                                break;
                            case 2:
                                v3 = carState.getLastSpeed();
                                System.out.println("v3 = " + v3);
                                if(v1 == 0) v1 = 1;
                                double k = (v1 - (v2 -v1))/ (v1*v1) * throttle;
                                double m = 1.0 / ( Math.log ( ( v3 - ( throttle / k ) ) / ( v2 - ( throttle / k ) ) ) / ( -k ) );
                                TrackKnowledge.getInstance().setMass(m);
                                TrackKnowledge.getInstance().setDragConstant(k);
                                massCalculated = true;
                                System.out.println("MASS = " + m + " Drag Const: " + k);
                                break;
                            default:
                                break;

                        }

                        send(new Throttle(throttle));
                    }
/////////////////////////////////// data collection
                /*    Data d = new Data();
                    d.acceleration = carState.getLastAcceleration();
                    d.slipAngle = carState.getLastSlipAngle();
                    d.speed = carState.getLastSpeed();
                    d.throttle = throttle;
                    d.tick = currentPositions.getGameTick();
                    double m = TrackKnowledge.getInstance().getMass();
                    d.mass = Double.isNaN(m) ? 0 : m;

                    double kk = TrackKnowledge.getInstance().getDragConstant();

                    d.k = Double.isNaN(kk) ? 0 : kk ;
                    d.force = TrackKnowledge.getInstance().getForce();
                    TrackPiece cp = track.getTrackPiece(currentPositions.getMyCarPosition().getTrackIndex());
                    if(cp.getType() == TrackPiece.PieceType.BEND)
                    {
                        d.isBend = true;
                        d.angle = cp.getAngle();
                        d.radius = cp.getRadius(track.getLane(currentPositions.getMyCarPosition().getEndLaneIndex()));



                    }

                    if(tick > 10)
                        data.add(d);

                    Main.LogLn("Throttle sent: " + throttle);*/
            	 ////////////////////////////////////////////
            		//update the car state
            		carState.updateCarState(currentPositions, throttle);
            	}
            	catch(Exception ex){
            		//ex.printStackTrace();
            		send(new Throttle(0.1));
            	}



            } 
            else if (msgFromServer.msgType.equals("crash")){
            	try{
                    //crashedAtLeastOne = true;
            		String crashCarName = (String)((LinkedTreeMap<String, Object>) msgFromServer.data).get("name");

            		if(crashCarName.equals(myCarName)){
            			System.out.println(tick+ ": Car crashed $#%@#$@@%# @#^$%$&%$%@ #$&%^$@#$$@$ #$%$#%^");
            		
            		carState.notifyTurboEnd();
            		carState.setCrashed(true);

                	Main.HIGHSPEED_SLIP = Main.HIGHSPEED_SLIP * TrackKnowledge.getInstance().FORCE_REDUCE_FACTOR;

                	TrackKnowledge.getInstance().notifyCrash(carState.getLastPieceIndex(),carState.getLastSpeed(), carState.getLastLaneIndex());

            		}
                    else{
                        TrackKnowledge.getInstance().removeOtherCarTurbo(crashCarName);
                    }
            	}
            	catch(Exception ex){
            		//ex.printStackTrace();
            	}

                send(new Ping());
            }
            else if(msgFromServer.msgType.equals("spawn")){
            	try{
            		String spawnedCarName = (String)((LinkedTreeMap<String, Object>) msgFromServer.data).get("name");

            		if(spawnedCarName.equals(myCarName)){
            			System.out.println(tick + " Car Spawned");
            			carState.setCrashed(false);
            		}
            	}
            	catch(Exception ex){
            		//ex.printStackTrace();
            		try{
            			carState.setCrashed(false);
            		}
            		catch(Exception ex2){
            			//ex2.printStackTrace();
            		}
            	}
            	send(new Ping());
            }
            else if (msgFromServer.msgType.equals("join")) {
                System.out.println(tick+ ": Joined");
                send(new Ping());
            } else if (msgFromServer.msgType.equals("gameInit")) {
            	track.LoadTrack(msgFromServer);
             
            	try{
            		if(carState!=null){
                    	carState.clearSwitchRequestHistory();
                    	carState.resetForRace();                    
                	}
            	}
            	catch(Exception ex){
            		//ex.printStackTrace();
            	}
               // SaveTextToFile(line);
                System.out.println(tick +": game init");
                send(new Ping());
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println(tick+": game end");
                send(new Ping());
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println(tick+":game start");
                send(new Ping());
            }            
            else if(msgFromServer.msgType.equals("yourCar")){
            	try {
            		myCarName = (String)((LinkedTreeMap<String, Object>) msgFromServer.data).get("name");
            		String color  = (String)((LinkedTreeMap<String, Object>) msgFromServer.data).get("color");
               		System.out.println("mycar: " + myCarName + " color:" + color);
            	}
            	catch(Exception ex){
            		//ex.printStackTrace();
            	}
                send(new Ping());
            }
            else if(msgFromServer.msgType.equals("turboAvailable")){
            	System.out.println(tick+ ": Turbo available");  // common for all cars
            	try{
            		LogLn(line);
            		Object factorObj = ((LinkedTreeMap<String, Object>) msgFromServer.data).get("turboFactor");
            		Object durationMSObj = ((LinkedTreeMap<String, Object>) msgFromServer.data).get("turboDurationMilliseconds");
            		Object durationTicksObj = ((LinkedTreeMap<String, Object>) msgFromServer.data).get("turboDurationTicks");
            	
            		double factor = factorObj != null ? (double)factorObj : 0;
            		double durationMS = durationMSObj != null ? (double)durationMSObj: 0;
            		double durationTicks = durationTicksObj != null ? (double) durationTicksObj : 0 ;
           	
            		carState.setTurboAvailable(factor, durationTicks, durationMS);
            	}
            	catch(Exception ex){
            		//ex.printStackTrace();
            	}
             //   send(new Ping());
            }
            else if(msgFromServer.msgType.equals("turboEnd")){
              //  send(new Ping());
            	try{
            		String relevantCarName = (String)((LinkedTreeMap<String, Object>) msgFromServer.data).get("name");

                	if(relevantCarName.equals(myCarName)){
                    	carState.notifyTurboEnd();
                    	System.out.println(tick+": Turbo ended........");
                	}
                	else{
                        TrackKnowledge.getInstance().removeOtherCarTurbo(relevantCarName);
                	}
            	}
            	catch(Exception ex){
            		//ex.printStackTrace();
            	}
            }
            else if(msgFromServer.msgType.equals("turboStart")){
            	try{
            		String relevantCarName = (String)((LinkedTreeMap<String, Object>) msgFromServer.data).get("name");

                	if(relevantCarName.equals(myCarName)){
                		send(new Throttle(carState.getLastThrottle())); // TODO: need to calculate throttle
                    	carState.notifyTurboStart();
                    	System.out.println(tick+": Turbo starts.......");
                	}
                	else{
                	//TODO Another car has started turbo
                	//    send(new Ping());
                        TrackKnowledge.getInstance().addOtherCarTurbo(relevantCarName);
                	}
            	}
            	catch(Exception ex){
            		//ex.printStackTrace();
            	}

            }
            else if(msgFromServer.msgType.equals("finish")){
                if(msgFromServer.data != null){
                	try{
                		Object obj = ((LinkedTreeMap<String, Object>) msgFromServer.data).get("name");
                    	if(obj != null){
                    	 
                        		String name = (String)obj;
                            	OpponentCars.getInstance().removeFinishedCar(name);

                        	 
                    	}
                	}
                	catch(Exception ex){
                		//ex.printStackTrace();
                	}

                }
            }
            else if(msgFromServer.msgType.equals("lapFinished")){
                //game tick is present in this message
                send(new Ping());

                if(msgFromServer.data != null){
                	try{
                		Object carObj = ((LinkedTreeMap<String, Object>) msgFromServer.data).get("car");
                		if(carObj != null){
                       
                				LinkedTreeMap<String, Object> carMap = (LinkedTreeMap<String, Object>)carObj;

                            if(carMap != null){
                                String name = (String) carMap.get("name");
                                if(name.equals(myCarName)){
                                    //We have finished a lap
                                    System.out.println(tick + ": Lap finished");
                                    TrackKnowledge.getInstance().recalculateAtEndOfLap();

                                }
                            }
                        }
                        
                    }
                	catch (Exception ex){
                        //ex.printStackTrace();
                    }

                }

                /*
                try{
                    tempLapCount ++;
                    if(tempLapCount == 2 && !crashedAtLeastOne)
                        WriteData(data);
                }
                catch (Exception ex)
                {
                    System.out.println("data write failed");
                }*/

            }
            else{
            	System.out.println("Unkwon messege???????????????????????????????????");
            	System.out.println(line);
                send(new Ping());
            }

        }
    }

    private void WriteData(ArrayList<Data> data) throws Exception{
        StringBuilder builder = new StringBuilder();
        for(Data d : data){
            builder.append(gson.toJson(d, Data.class) + "\n");
        }

        String fn = UUID.randomUUID().toString();
        fn = trackName + fn;
        PrintWriter writer = new PrintWriter("data/"+fn+".data", "UTF-8");
        writer.print(builder.toString());
        writer.close();
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();

     //  if(msg.msgType().equals("createRace") || msg.msgType().equals("joinRace")
     //           || msg.msgType().equals("switchLane"))
     //       Main.LogLn(msg.toJson());
    }

    public static void LogLn(String str){
       if(debug)
        System.out.println(tick + ": "+str);
    }
    
    public void SaveTextToFile(String txt){
    	try{

            PrintWriter writer = new PrintWriter("temp.txt", "UTF-8");
            writer.print(txt);
            writer.close();
    	}
    	catch(Exception ex){
    		
    	}
    }


    
   
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {

    private final String name;
    private final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }

}

class CreateRace extends SendMsg{

    public String msgType = "createRace";
    public LinkedTreeMap<String, Object> data = new LinkedTreeMap<>();

  //  private final String password = "rukshan";



    CreateRace(String name, String key, String trackName, Boolean create, int count, String password){
        LinkedTreeMap<String, Object> botId = new LinkedTreeMap<>();
        botId.put("name",name);
        botId.put("key",key);
        data.put("botId",botId);
        data.put("trackName", trackName);
        data.put("carCount", count);

        if(!password.isEmpty())
            data.put("password", password);

        if(!create)
            msgType ="joinRace";

    }





    @Override
    protected String msgType() {
        return msgType;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this);
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class TurboMessage extends SendMsg {
	
	
	@Override
	protected String msgType(){
		return "turbo";
	}
	
	@Override
	 protected Object msgData() {
	        return "Mora power!";
	    }
}


class SwitchMessage extends SendMsg {
	
	private int direction = 0 ;
	
	SwitchMessage(int dir){
		this.direction = dir;
	}
	
	@Override
	protected String msgType(){
		return "switchLane";
	}
	
	@Override
	protected Object msgData() {
	        if(direction > 0)
	        	return "Right";
	        else
	        	return "Left";
	}
	
	 
}