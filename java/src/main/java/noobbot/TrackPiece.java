package noobbot;

public class TrackPiece {

	public enum PieceType 
	{
		STRAIGHT,
		BEND
	}
	
	private PieceType type;
	private int index; // position
	private double length;
	private double angle;
    private double radius;
    private boolean canSwitch = false ;
	
	public TrackPiece(int index, PieceType type, double length, double angle, boolean canSwitch)
	{
		this.setIndex(index);
		this.setType(type);
		this.canSwitch = canSwitch;
				
		if(type == PieceType.STRAIGHT)
			this.setLength(length);
		else
		{
			this.angle = angle;
            this.radius = length;
		}
			
	}
	
	public PieceType getPieceType(){
		return this.getType();
	}

	double getLength(Lane lane) {
        if(this.getType() == PieceType.STRAIGHT)
		    return length;
        else
        {
            if(angle > 0 )
                return (Math.abs(angle)  * Math.PI * (radius - lane.getDistanceFromCenter()) / 180);
            else
                return (Math.abs(angle)  * Math.PI * (radius + lane.getDistanceFromCenter()) / 180);
        }
	}

    double getCenterLength(){
        if(this.getType() == PieceType.STRAIGHT)
            return length;
        else
        {
            return (Math.abs(angle)  * Math.PI * (radius) / 180);
        }
    }

    double getRadius(Lane lane) {
            if(angle > 0 )
                return radius - lane.getDistanceFromCenter();
            else
                return radius + lane.getDistanceFromCenter();

    }

	void setLength(double length) {
		this.length = length;
	}

	int getIndex() {
		return index;
	}

	void setIndex(int index) {
		this.index = index;
	}

	PieceType getType() {
		return type;
	}

	void setType(PieceType type) {
		this.type = type;
	}

	public double getAngle() {
		return angle;
	}

	public void setAngle(double angle) {
		this.angle = angle;
	}

    public double getRadius(){
        return radius;
    }

	public boolean isCanSwitch() {
		return canSwitch;
	}

	public void setCanSwitch(boolean canSwitch) {
		this.canSwitch = canSwitch;
	}

}
