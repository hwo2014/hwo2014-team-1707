package noobbot;

/**
 * Created by rukshan on 4/18/14.
 */
public class Lane {
    private int index;
    private double distanceFromCenter;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public double getDistanceFromCenter() {
        return distanceFromCenter;
    }

    public void setDistanceFromCenter(double distanceFromCenter) {
        this.distanceFromCenter = distanceFromCenter;
    }
}
