package noobbot;

import java.util.HashMap;


import noobbot.TrackPiece.PieceType;

public class CarState {

    private double mass;
    private double dragConstant;
	private double lastTick;
	private int lastPieceIndex;
	private double lastInPieceDistance;
	private double lastSlipAngle;
	private int lastLap;
	private double lastThrottle;
    private int lastLaneIndex;
    private double lastAcceleration;
    private boolean isCrashed= false;
    private boolean wastedTurbo = false;
    double currentTurboFactor = 1.0;
    double proposedTurboFactor = 1.0;

    private double lastSpeed = 0 ; //distance per tick
    
    private Turbo turboAvailable ;
    private boolean inTurbo ;

    private HashMap<String,Object> switchRequestsMade = new HashMap<>();

    public void clearSwitchRequestHistory(){
        switchRequestsMade.clear();
    }

    //TODO : qualifying round always has lap - 0 but there could be more than one lap

    public boolean hasAlreadyRequested(double lap , double switchIndex){
        String key = lap + "#" + switchIndex;
        if(switchRequestsMade.containsKey(key))
            return true;
        else
            return false;

    }

    public void addSwitchRequest(double lap, double switchIndex){
        String key = lap + "#" + switchIndex;

        if(!switchRequestsMade.containsKey(key))
            switchRequestsMade.put(key, true);
    }
    
    
  //  public boolean shouldUseTurbo(){
  //  	return turnOnTrubo;
  //  }
    
  //  public void ackTurnTurboOn(){
  //  	this.turnOnTrubo = false;
  //  }
    
    public void notifyTurboStart(){
    	this.inTurbo = true ;
        currentTurboFactor = proposedTurboFactor;
    //	turboAvailable = null;
    }
    
    public void notifyTurboEnd(){
    	this.inTurbo = false ;
        currentTurboFactor = 1.0;
    }
    
    public void setTurboAvailable(double factor, double durationTicks, double durationMS){
        if(isCrashed)
            return ;

        if(turboAvailable != null)
            wastedTurbo = true;

    	Turbo t = new Turbo();
    	t.turboDurationMilliseconds = durationMS;
    	t.turboDurationTicks = durationTicks;
    	t.turboFactor = factor;
    	
    	turboAvailable = t;
    }
    
    public void resetForRace(){
    	this.lastAcceleration = 0 ;
    	this.lastInPieceDistance = 0 ;
    	this.lastLaneIndex = 0 ;
    	this.lastLap = 0 ;
    	this.lastPieceIndex = 0 ;
    	this.lastSlipAngle = 0 ;
    	this.lastSpeed = 0 ;
    	this.lastThrottle = 0 ;
    	this.lastThrottle = 0 ;
    	this.isCrashed = false;
    	this.inTurbo = false;
    }

	
	public void updateCarState(CarPositions carPositions, double throttle){
		
		CarPosition myCar = carPositions.getMyCarPosition();

		this.lastTick = carPositions.getGameTick();
		this.lastPieceIndex = myCar.getTrackIndex();
		this.lastInPieceDistance = myCar.getInPieceDistance();
        this.lastSlipAngle = myCar.getSlipAngle();
        this.setLastLaneIndex(myCar.getEndLaneIndex());
		this.setLastThrottle(throttle);
		
		
	}

    public double getSpeedDifference(double currentSpeed){
        return  currentSpeed - getLastSpeed();
    }

    public int getSuggestedSwitch(CarPositions carPositions, TrackHelper track){
    	 CarPosition myCurrentPos = carPositions.getMyCarPosition();
    	 
    	TrackPiece closestSwitch = track.GetNextSwitchPiece(myCurrentPos.getTrackIndex(), myCurrentPos.getLap());
    	 
    	

        if(closestSwitch == null){
            return 0;
        }
        
        double distanceToSwitch = track.getDistance(track.getTrackPiece(myCurrentPos.getTrackIndex()), closestSwitch, myCurrentPos.getInPieceDistance(), track.getLane(myCurrentPos.getEndLaneIndex()));

        double switchDistance = inTurbo ? TrackKnowledge.DefaultMaxSpeed * 3 : TrackKnowledge.DefaultMaxSpeed * 2;
        if(TrackKnowledge.DefaultMaxSpeed *2< distanceToSwitch){
        	//we can decide later
        	return 0;
        }

         if(hasAlreadyRequested(myCurrentPos.getLap(), closestSwitch.getIndex()))
             return 0;

    	 int dir =  track.getHighestBendsTillNextSwitch(closestSwitch, myCurrentPos.getLap());

         TrackPiece nextSwitchPiece = track.GetNextSwitchPiece(closestSwitch.getIndex(), myCurrentPos.getLap());
         dir = OpponentCars.getInstance().bestLaneDirFromOnePieceToAnotherPiece(dir, track.getLane(myCurrentPos.getEndLaneIndex()), lastSpeed, track.getTrackPiece(myCurrentPos.getTrackIndex()), nextSwitchPiece,myCurrentPos.getLap(), TrackKnowledge.getInstance().getMass());
    	 
    	if(dir == 0)
    		 return 0;

        /*
    	 int numberOfLanes  = track.getLanes().size();
    	 
    	 int startLane = myCurrentPos.getStartLaneIndex();
    	 int endLane = myCurrentPos.getEndLaneIndex();


    	 if(startLane != endLane){
    		 return 0; //we are already switching
         }
    	 
    	 if(dir > 0  && numberOfLanes - 1  <= startLane){
    		 return 0;
         }
    	 
    	 if(dir < 0 && startLane <= 0){
    		 return 0;
         }

    	 int switchLaneIndex = dir > 0 ? endLane + 1 : endLane -1;
    	 
    	 Lane  switchLane = track.getLane(switchLaneIndex);
    	 
    	 if(switchLane != null){
    	     TrackPiece nextSwitchPiece = track.GetNextSwitchPiece(closestSwitch.getIndex(), myCurrentPos.getLap());

    		 if(!OpponentCars.getInstance().isLaneClearTillNextSwitch(closestSwitch, nextSwitchPiece, switchLane, lastSpeed)){
                 System.out.println(carPositions.getGameTick() + ": Decided against switch!");
    			 return 0;
    		 }
    	 }*/
    		 
    	 
         addSwitchRequest(myCurrentPos.getLap(),closestSwitch.getIndex());

    	 return dir;
    	 
    }
    


    public boolean shouldUseTurbo(CarPositions carPositions, double throttle){

        if(isCrashed)
            return false ;

        if(turboAvailable == null)
            return false;


        if(throttle <= 0.5 && !wastedTurbo){
        	//has a better situation in the future to use turbo??
            try {
        	    if(TrackHelper.getInstance().hasBetterPositionForTurbo(carPositions)){
        		    return false;
                }
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
    	//check the track future to see if there is a place to do turbo if throttle is less than 0.5



        CarPosition myCurrentPos = carPositions.getMyCarPosition();

        TrackPiece currentTrackPiece = TrackHelper.getInstance().getTrackPiece(myCurrentPos.getTrackIndex());

        //if it is a bend do not use Turbo
        if(currentTrackPiece.getType() == PieceType.BEND)
            return false;
        TrackHelper track = TrackHelper.getInstance();
        Lane myLane = track.getLane(myCurrentPos.getEndLaneIndex());
        TrackPiece nextBend = track.getNexBendTrackPiece(currentTrackPiece.getIndex(), myCurrentPos.getLap());

        try {
            if(shouldEnableTurboFromStraightPiece(throttle, track, carPositions, currentTrackPiece, nextBend, myLane))
            {
                Main.LogLn("Last few pieces or bends are far away");
                //there is no bend before the end of the last lap
                if(Math.abs( myCurrentPos.getSlipAngle() ) < TrackKnowledge.getInstance().MAX_SLIP ){
                    proposedTurboFactor = turboAvailable.turboFactor;
                    turboAvailable = null;
                    return true;
                }

            }

        }
        catch (Exception ex){
            ex.printStackTrace();
        }

        return false;
    }


    //suggest thrott
    public double getSuggestedThrottleV2(CarPositions carPositions, TrackHelper track){
        double suggestedThrottle = lastThrottle;

        CarPosition myCurrentPos = carPositions.getMyCarPosition();
        TrackPiece lastPiece = track.getTrackPiece(this.lastPieceIndex);
        TrackPiece currentTrackPiece = track.getTrackPiece(myCurrentPos.getTrackIndex());

        double distanceTravelled = track.getDistanceTravelled(lastPieceIndex, currentTrackPiece.getIndex(),lastInPieceDistance, myCurrentPos.getInPieceDistance(),myCurrentPos.getEndLaneIndex());
      



        if(currentTrackPiece.getType() == PieceType.BEND){
        	
        	//turnOffTurbo();
        	TrackPiece nextBendPiece = track.getNexBendTrackPiece(currentTrackPiece.getIndex(), myCurrentPos.getLap());
        	
        	double distanceToNextBend = Double.MAX_VALUE;
        	
        	if(nextBendPiece != null && nextBendPiece.getType() == PieceType.BEND)
        		distanceToNextBend = track.getDistance(currentTrackPiece, nextBendPiece, myCurrentPos.getInPieceDistance(), track.getLane(myCurrentPos.getEndLaneIndex()));
        	else
        		nextBendPiece = null ; // it is not a bend
            //we are in a bend
            suggestedThrottle = getBendThrottle(carPositions,lastPiece,currentTrackPiece,distanceTravelled,distanceToNextBend, track.getLane(myCurrentPos.getEndLaneIndex()),nextBendPiece);
        }
        else
        {
        	  double distanceToNextPieceType = track.getDistanceToNextTrackType(myCurrentPos.getTrackIndex(),myCurrentPos.getInPieceDistance(), myCurrentPos.getEndLaneIndex());
            //we are in a straight track
            suggestedThrottle = getStraightThrottle(carPositions, lastPiece, currentTrackPiece, distanceTravelled, distanceToNextPieceType, track.getLane(myCurrentPos.getEndLaneIndex()), track);
        }


        setLastAcceleration(distanceTravelled - getLastSpeed());
        setLastSpeed(distanceTravelled);

        return  suggestedThrottle;
    }

    //Get throttle for bend
    private double getBendThrottle(CarPositions carPositions, TrackPiece lastPiece, TrackPiece currentPiece, double distanceTravelled, double distanceToNextBend, Lane myLane, TrackPiece nextBendPiece){

    	
    	if(inTurbo){
    		currentTurboFactor = turboAvailable.turboFactor ;
    	}
    	
    	CarPosition myPos = carPositions.getMyCarPosition();

        TrackKnowledge.getInstance().notifySlip(currentPiece.getIndex(),myPos.getSlipAngle(), myLane.getIndex(), distanceTravelled, lastThrottle, myPos.getLap());
        double throttle = 0.1 ;
        try
        {
            double maxSpeedInCurrent = TrackKnowledge.getInstance().maxPossibleSpeed(currentPiece.getIndex(), currentPiece.getRadius(myLane));
         //  double maxSpeedInCurrent = DataProcessor.getInstance().getForceUsingSvm(currentPiece.getRadius(myLane), currentPiece.getAngle(), TrackKnowledge.getInstance().getMaxSlipForBend(currentPiece.getIndex()),getMass());

           if( distanceTravelled > maxSpeedInCurrent) {
               Main.LogLn("HIGH HIGH HIGH ");
               // we are going at a higher speed. No more gas
               if(Main.HIGHSPEED_SLIP > Math.abs(carPositions.getMyCarPosition().getSlipAngle()) && !(isTooFastForBendToBend(distanceTravelled, distanceToNextBend, nextBendPiece, myLane)))
                   throttle = TrackKnowledge.getInstance().getThrottle(distanceTravelled, maxSpeedInCurrent,1,currentTurboFactor);
               else
                    throttle = 0.0;
               Main.LogLn(carPositions.getGameTick() + " BEND: max "+ maxSpeedInCurrent+" < cur speed "+  distanceTravelled+"  t->"+ throttle);
           }
           else{
        	   
        	   //current speed is less then max speed for the bend
        		
        	   if(isTooFastForBendToBend(distanceTravelled, distanceToNextBend, nextBendPiece, myLane)){
        		   //possibly moving to next bend
                   // we have to slow down


                   double nextBendMaxSpeed = TrackKnowledge.getInstance().maxPossibleSpeed(nextBendPiece.getIndex(), nextBendPiece.getRadius(myLane));

                   throttle =  TrackKnowledge.getInstance().getThrottle(distanceTravelled, nextBendMaxSpeed,1, currentTurboFactor);

                   Main.LogLn(carPositions.getGameTick() + " BEND TO BEND: Too fast for the next bend t->" + throttle);

               }
        	   else if(TrackKnowledge.getInstance().getDangerSlip(myPos.getTrackIndex()) > Math.abs(myPos.getSlipAngle())) {

                    if(myPos.getSlipAngle() == 0.0){
                        throttle =  TrackKnowledge.getInstance().getThrottle(distanceTravelled, maxSpeedInCurrent,1, currentTurboFactor);
                    }
                    else{

                        double v = Math.cos(toRadians(myPos.getSlipAngle())) * maxSpeedInCurrent ;
                        throttle =  TrackKnowledge.getInstance().getThrottle(distanceTravelled, v,1, currentTurboFactor);
                        Main.LogLn("There is a slip. Effective max speed: " + v);
                    }
                    Main.LogLn("speed < max, not too fast for B to B, less then danger slip");
                    Main.LogLn(carPositions.getGameTick() + " BEND: max "+ maxSpeedInCurrent+" > cur speed "+  distanceTravelled+"  t->" + throttle);
               }
               else
               {

                   throttle = 0.0;

                   Main.LogLn(carPositions.getGameTick() + " BEND: Danger Slip angle " + carPositions.getMyCarPosition().getSlipAngle() + "  t->" + throttle);
               }
           }
        }
        catch (Exception ex)
        {
            System.out.println("Bend Throttle calculation: Bend max speed failed");
        }
        return throttle;
    }
    
    private boolean isTooFastForBendToBend(double currentSpeed, double distanceToNextBend, TrackPiece nextBendPiece, Lane myLane){
    	
    	double currentTurboFactor = 1.0;
    	
    	if(inTurbo)
    		currentTurboFactor = turboAvailable.turboFactor;
    	
    	if(nextBendPiece == null)
    		return false ;
      double nextBendMaxSpeed = Double.MAX_VALUE;
 	   
      try
      {
 	   if(nextBendPiece != null){
            nextBendMaxSpeed = TrackKnowledge.getInstance().maxPossibleSpeed(nextBendPiece.getIndex(), nextBendPiece.getRadius(myLane));
 		   //  nextBendMaxSpeed = DataProcessor.getInstance().getForceUsingSvm(nextBendPiece.getRadius(myLane), nextBendPiece.getAngle(), TrackKnowledge.getInstance().getMaxSlipForBend(nextBendPiece.getIndex()),getMass());
       }


 	    if(nextBendMaxSpeed < currentSpeed)
 	    {
           double maxSpeedForNext = TrackKnowledge.getInstance().maxPossibleSpeed(nextBendPiece.getIndex(), nextBendPiece.getRadius(myLane));
 	       //too fast for next bend
           //but the next bend can bee far away. There could be straight pieces before that
           double distanceInNextTickWithMaxThrottle = TrackKnowledge.getInstance().speedInGivenAmountOfTicks(1, currentSpeed, 1,currentTurboFactor);

            if( distanceInNextTickWithMaxThrottle < distanceToNextBend  ){
                //max-throttle-speed won't take us to the next
                if(distanceInNextTickWithMaxThrottle < maxSpeedForNext){
                    //max-throttle-speed is less than the next bend speed
                    return false;
                }
                else
                {
                    //max bend speed is less than max-throttle-speed in next tick (too fast for next)
                    //we have to slow down in time
                    //    double ticksRequiredToSlow = (distanceTravelled - maxSpeed ) / TrackKnowledge.DefaultDecreasePerSec ;
                    int tickReqToSlow = TrackKnowledge.getInstance().ticksRequiredToGetToGivenSpeed(currentSpeed, maxSpeedForNext, 0, currentTurboFactor);

                    //  double distanceInTickReq = ticksRequiredToSlow * distanceTravelled ; // how long it will go at current speed
                    double distReq = TrackKnowledge.getInstance().distanceTravelledInTicks(tickReqToSlow, currentSpeed, 0, currentTurboFactor);

                    double remainingDistance = distanceToNextBend - distanceInNextTickWithMaxThrottle;
                    // double zeroThrottleDistance = TrackKnowledge.getInstance().distanceTravelledInTicks(1, distanceInNextTickWithMaxThrottle, 0);

                    if(distReq >= distanceToNextBend){
                        //we are in trouble. Even zero throttle will take us to the bend with a high speed
                        return true;
                    }
                    else
                    {

                            int t = TrackKnowledge.getInstance().ticksRequiredToGetToGivenSpeed(distanceInNextTickWithMaxThrottle, maxSpeedForNext, 0, currentTurboFactor);

                            //  double distanceInTickReq = ticksRequiredToSlow * distanceTravelled ; // how long it will go at current speed
                            double dr = TrackKnowledge.getInstance().distanceTravelledInTicks(t, distanceInNextTickWithMaxThrottle, 0, currentTurboFactor);

                            if(remainingDistance > dr ){
                                return false;
                            }
                            else{

                                return true;
                            }

                    }
                }
            }
            else if(distanceInNextTickWithMaxThrottle >= distanceToNextBend){
                //max-throttle speed will take us to the next
                //we will go to the next piece with max throttle
                if(nextBendPiece != null && TrackKnowledge.getInstance().hasCrashedBefore(nextBendPiece.getIndex()))
                    return true;

                if(maxSpeedForNext > distanceInNextTickWithMaxThrottle)
                {
                   return false;
                }
                else if(maxSpeedForNext > -1)
                {
                    return true;
                }

            }

 		   return true;
 	    }
      }
      catch(Exception ex)
      {
    	  ex.printStackTrace();
      }

      Main.LogLn("BEND TO BEND.. OK. Next Bend Max Speed: " + nextBendMaxSpeed + " current speed: " + currentSpeed + " distance to next: " + distanceToNextBend);
      return false ;
    }

    private double getStraightThrottle(CarPositions carPositions, TrackPiece lastPiece, TrackPiece currentPiece, double distanceTravelled, double distanceToNextBend, Lane myLane, TrackHelper track){
        

    	
    	
    	
        double throttle = 0.3 ;
        CarPosition myCarPosition = carPositions.getMyCarPosition();

        double maxSpeedForNext = -1.0;
        TrackPiece nextBend = track.getNexBendTrackPiece(currentPiece.getIndex(), myCarPosition.getLap());
        try{


            if(nextBend == null){
                if(Math.abs( myCarPosition.getSlipAngle() ) < Main.STRAIGHT_MAX_SLIP)
            	    return 1.0;
                else
                    return 0.5;
            }

            maxSpeedForNext = TrackKnowledge.getInstance().maxPossibleSpeed(nextBend.getIndex(), nextBend.getRadius(myLane));
          //  maxSpeedForNext = DataProcessor.getInstance().getForceUsingSvm(nextBend.getRadius(myLane), nextBend.getAngle(), TrackKnowledge.getInstance().getMaxSlipForBend(nextBend.getIndex()), getMass()) ;
        }
        catch (Exception ex){
            System.out.println("Straight Throttle calculation: Bend max speed failed");
            ex.printStackTrace();
        }

        double distanceInNextTickWithMaxThrottle = TrackKnowledge.getInstance().speedInGivenAmountOfTicks(1, distanceTravelled, 1, currentTurboFactor);

        //distanceTravelled + 0.1 < distanceToNextBend  (reconsider)
        if( distanceInNextTickWithMaxThrottle < distanceToNextBend  ){
             //max-throttle-speed won't take us to the next
            if(distanceInNextTickWithMaxThrottle < maxSpeedForNext){
                //max-throttle-speed is less than the next bend speed
                if(Math.abs( myCarPosition.getSlipAngle() ) < Main.STRAIGHT_MAX_SLIP)
                    throttle = 1;
                else
                    throttle = 0.1;

                Main.LogLn("STRAIGHT: max + cur speed "+distanceTravelled + "  < distance to travel and MaxBendSPeed " + maxSpeedForNext+ " > Cur Speed t->"+throttle);
            }
            else
            {
                //max bend speed is less than max-throttle-speed in next tick (too fast for next)
                //we have to slow down in time
            //    double ticksRequiredToSlow = (distanceTravelled - maxSpeed ) / TrackKnowledge.DefaultDecreasePerSec ;
                int tickReqToSlow = TrackKnowledge.getInstance().ticksRequiredToGetToGivenSpeed(distanceTravelled, maxSpeedForNext, 0, currentTurboFactor);

              //  double distanceInTickReq = ticksRequiredToSlow * distanceTravelled ; // how long it will go at current speed
                double distReq = TrackKnowledge.getInstance().distanceTravelledInTicks(tickReqToSlow, distanceTravelled, 0, currentTurboFactor);

                double remainingDistance = distanceToNextBend - distanceInNextTickWithMaxThrottle;
               // double zeroThrottleDistance = TrackKnowledge.getInstance().distanceTravelledInTicks(1, distanceInNextTickWithMaxThrottle, 0);
                Main.LogLn("tick r: " +tickReqToSlow+  " distance to next bend:" + distanceToNextBend + " rem distance: " + remainingDistance + " Dist req to slow:" + distReq);

                if(distReq >= distanceToNextBend){
                    //we are in trouble. Even zero throttle will take us to the bend with a high speed
                    throttle = 0.0 ;
                    Main.LogLn("STRAIGHT: Start slowing down before bend t->" + throttle);
                }
                else
                {
                    // Zero throttle will make sure we slow down to next-speed before we get there
                    if(Math.abs(myCarPosition.getSlipAngle()) < TrackKnowledge.getInstance().getDangerSlip(myCarPosition.getTrackIndex())
                    		|| !TrackKnowledge.getInstance().hasCrashedBefore(myCarPosition.getTrackIndex())){

                        int t = TrackKnowledge.getInstance().ticksRequiredToGetToGivenSpeed(distanceInNextTickWithMaxThrottle, maxSpeedForNext, 0, currentTurboFactor);

                        //  double distanceInTickReq = ticksRequiredToSlow * distanceTravelled ; // how long it will go at current speed
                        double dr = TrackKnowledge.getInstance().distanceTravelledInTicks(t, distanceInNextTickWithMaxThrottle, 0, currentTurboFactor);

                        if(remainingDistance > dr ){
                            throttle = 1;
                        }
                        else{

                            throttle = TrackKnowledge.getInstance().getThrottle(distanceTravelled, distanceToNextBend - dr,1, currentTurboFactor);
                        }
                        Main.LogLn("STRAIGHT: Enough time to slow down before bend t->" + throttle);
                	}
                	else{
                		throttle = 0.01;
                        Main.LogLn("STRAIGHT: Can slow before bend but there is a slip. t->" + throttle);
                	}
                }
            }
        }
        else if(distanceInNextTickWithMaxThrottle >= distanceToNextBend){
            //max-throttle speed will take us to the next
            //we will go to the next piece with max throttle
          //  if(nextBend != null && TrackKnowledge.getInstance().hasCrashedBefore(nextBend.getIndex()))
          //      return 0.0;

            if(maxSpeedForNext > distanceInNextTickWithMaxThrottle)
            {
                if(Math.abs( myCarPosition.getSlipAngle() ) < Main.STRAIGHT_MAX_SLIP
                		|| !TrackKnowledge.getInstance().hasCrashedBefore(myCarPosition.getTrackIndex()))
                    throttle = 1;
                else
                    throttle = 0.5;

                Main.LogLn("STRAIGHT: Max Throttle got to next. max  > distance travelled t->" + throttle);
            }
            else if(maxSpeedForNext > 0)
            {

                // we have to slow down
                throttle = TrackKnowledge.getInstance().getThrottle(distanceTravelled, maxSpeedForNext,1, currentTurboFactor);
                Main.LogLn("STRAIGHT: max < distance travelled  t->" + throttle);
            }

        }
        /*(else if(distanceTravelled < distanceToNextBend){
            //we can do better
            if(maxSpeed > distanceTravelled){
                throttle = 1;

                Main.LogLn("STRAIGHT:  distance travelled < distance to next and maxSpeed > cur speed. t->1");
            }
            else
            {
                throttle = 0.1;
                Main.LogLn("STRAIGHT:  distance travelled < distance to next but maxSpeed < cur speed. t->0");
            }
        } */

        return  throttle;
    }
    
    
    
  /*  private void enableTurbo(){
    	if(turboAvailable != null && !inTurbo){
        	turnOnTrubo = true; 
         }   
    	else{
    		turnOnTrubo = false;
    	}
    }
    
    private void turnOffTurbo(){
    	turnOnTrubo = false ;
    }*/
    
    private boolean shouldEnableTurboFromStraightPiece(double throttle, TrackHelper track, CarPositions carPositions, TrackPiece currentPiece, TrackPiece nextBend, Lane lane){
        CarPosition myCarPosition = carPositions.getMyCarPosition();
    	if(currentPiece.getType() == PieceType.BEND)
    		return false ;
    	
    	if(turboAvailable == null) {// there is no turbo to enable
    		return false ;
    	}
    	
    	if(inTurbo) // we are already in turbo mode
    		return false ; // 
    	    	
    	if(nextBend == null)
    		return true;
    	

        TrackKnowledge trackKnowledge = TrackKnowledge.getInstance();

    	double distanceToNextBend = track.getDistance(currentPiece, nextBend, myCarPosition.getInPieceDistance(), lane);

        double turboDuration = turboAvailable.getTurboDuratoin();

    	double maxDistanceInTurbo = TrackKnowledge.getInstance().distanceTravelledInTicksWithTurbo((int)  turboDuration, this.lastSpeed, throttle, turboAvailable.turboFactor);

		Main.LogLn("Distance to next bend: " + distanceToNextBend + " turbo dist: " + maxDistanceInTurbo);

    	if(maxDistanceInTurbo < distanceToNextBend){
           /* double terminalSpeed =   trackKnowledge.speedInGivenAmountOfTicks((int) turboDuration, this.lastSpeed, throttle, turboAvailable.turboFactor);

            double maxSpeedInBend = trackKnowledge.maxPossibleSpeed(nextBend.getIndex(), nextBend.getRadius(lane));

            if(terminalSpeed < maxSpeedInBend)
                return true;

            int timeReqToSlowDown = trackKnowledge.ticksRequiredToGetToGivenSpeed(terminalSpeed, maxSpeedInBend,0.0,1.0);
            double distanceReq = trackKnowledge.distanceTravelledInTicks( timeReqToSlowDown, terminalSpeed, 0.0, 1.0);

            if(distanceReq < (distanceToNextBend - maxDistanceInTurbo) )
                return true;
            else if(canDependOnOtherCarForTurbo(carPositions, turboDuration, maxDistanceInTurbo))
                return true; */

            return true;

    	}
    	else
    	{
    		if(canDependOnOtherCarForTurbo(carPositions, turboDuration, maxDistanceInTurbo))
                return true;
    		//todo: if there is a car within that distance. turbo on!!!
    	}
    	
    	return false ;
    }

    boolean canDependOnOtherCarForTurbo(CarPositions carPositions, double turboDuration, double turboDistance){

        try {
            CarPosition myCar = carPositions.getMyCarPosition();
            int myLaneIndex = myCar.getEndLaneIndex();
            TrackHelper trackHelper = TrackHelper.getInstance();
            TrackKnowledge trackKnowledge = TrackKnowledge.getInstance();
            //is there a swith within turbo distance
            if(trackHelper.hasSwitch(myCar, turboDistance))
                return false;

            TrackPiece next = trackHelper.getTrackPiece(myCar.getTrackIndex());

            for(String opName : carPositions.getOtherCarPositions().keySet()){

                CarPosition op = carPositions.getOtherCarPositions().get(opName);

                if(op == null)
                    continue;;

                if(op.getEndLaneIndex() != myLaneIndex)
                    continue;

                if(op.getTrackIndex() != myCar.getTrackIndex())
                    continue;

                if(op.getInPieceDistance() > myCar.getInPieceDistance()){
                    //car in front of me
                    if(!trackKnowledge.isOtherCarInTurbo(opName) && ( (op.getInPieceDistance() -myCar.getInPieceDistance() )< turboDistance ))
                        return true;
                }
            }

         /*   int i = 0 ;

            while(i < trackHelper.getPieceCount() && next != null){
                i++;
                break;

            } */


        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return false;
    }

	
	double getLastTick() {
		return lastTick;
	}
	void setLastTick(double lastTick) {
		this.lastTick = lastTick;
	}
	int getLastPieceIndex() {
		return lastPieceIndex;
	}
	void setLastPieceIndex(int lastPieceIndex) {
		this.lastPieceIndex = lastPieceIndex;
	}
	double getLastInPieceDistance() {
		return lastInPieceDistance;
	}
	void setLastInPieceDistance(double lastInPieceDistance) {
		this.lastInPieceDistance = lastInPieceDistance;
	}
	double getLastSlipAngle() {
		return lastSlipAngle;
	}
	void setLastSlipAngle(double lastSlipAngle) {
		this.lastSlipAngle = lastSlipAngle;
	}
	int getLastLap() {
		return lastLap;
	}
	void setLastLap(int lastLap) {
		this.lastLap = lastLap;
	}

	public double getLastThrottle() {
		return lastThrottle;
	}

	public void setLastThrottle(double lastThrottle) {
		this.lastThrottle = lastThrottle;
	}

    public String toString(){
        StringBuilder builder = new StringBuilder();
        builder.append("Car State *******************************************************\n");
        builder.append("Last Throttle: " + getLastThrottle() +"\n");
        builder.append("Last Tick: "+ getLastTick() +"\n");
        builder.append("Last Slip Angle: "+getLastSlipAngle()+"\n");
        builder.append("Last In Piece Distance: "+getLastInPieceDistance()+"\n");
        builder.append("End of Car State ************************************************\n");

        return builder.toString();
    }

    public double getLastAcceleration() {
        return lastAcceleration;
    }

    public void setLastAcceleration(double lastAcceleration) {
        this.lastAcceleration = lastAcceleration;
    }

    public int getLastLaneIndex() {
        return lastLaneIndex;
    }

    public void setLastLaneIndex(int lastLaneIndex) {
        this.lastLaneIndex = lastLaneIndex;
    }

    public double getLastSpeed() {
        return lastSpeed;
    }

    public void setLastSpeed(double lastSpeed) {
        this.lastSpeed = lastSpeed;
    }

	public boolean isCrashed() {
		return isCrashed;
	}

	public void setCrashed(boolean isCrashed) {
		this.isCrashed = isCrashed;
	}
 

    public double toRadians(double degrees){
        return degrees * Math.PI / 180;
    }



}

class Turbo {
	public double turboFactor;
	public double turboDurationTicks;
	public double turboDurationMilliseconds ;
	
	public double getMaxSpeedIncrease(double throttle)
	{
		if(throttle > 1) throttle = 1;
		
		return  (turboFactor - 1 ) * 0.2 * throttle ;
	}
	
	public double getTurboDuratoin(){
		if(turboDurationTicks <= 0){
			turboDurationTicks = 30 * turboDurationMilliseconds / 500 ;
		}

        return Math.ceil(turboDurationTicks);

	}
}
