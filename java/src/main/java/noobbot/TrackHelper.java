package noobbot;

import java.util.ArrayList;

import noobbot.TrackPiece.PieceType;

import com.google.gson.internal.LinkedTreeMap;

public class TrackHelper {

	ArrayList<TrackPiece> pieces = new ArrayList<>();
    private ArrayList<Lane> lanes = new ArrayList<>();
    double totalLaps = 0 ;
    double totalTrackCenterLength = 0.0;
    final double TURBO_POSITION_CHECK_RATIO = 0.5 ;
    int futureBestTurboIndex = -1;
    
    private static TrackHelper instance ;
    
    private TrackHelper(){
    	
    }
    
    public double getTotalLaps(){
    	return totalLaps;
    }
    
    public static TrackHelper getInstance(){
    	if(instance == null)
    		instance = new TrackHelper();
    	
    	return instance;
    }
    
	public void LoadTrack(MsgWrapper msgFromServer)
	{
		pieces = new ArrayList<>();
	    lanes = new ArrayList<>();
	    
        LinkedTreeMap<String, Object> tm = (LinkedTreeMap<String, Object>)msgFromServer.data;
        LinkedTreeMap<String, Object> racemap = (LinkedTreeMap<String, Object>)tm.get("race");
        LinkedTreeMap<String, Object> trackm = (LinkedTreeMap<String, Object>)racemap.get("trackName");
        LinkedTreeMap<String, Object> raceSession = (LinkedTreeMap<String, Object>)racemap.get("raceSession");

        if(raceSession != null){ // race session is null in qualifying round
        	Object lapsObj = raceSession.get("laps");
        	if(lapsObj != null)
        		totalLaps = (double)lapsObj;
        }

        if(trackm == null)
        {
            trackm = (LinkedTreeMap<String, Object>)racemap.get("track");
        }
        
        ArrayList<Object> pal = (ArrayList<Object>) trackm.get("pieces");
        
        int i = 0 ;
        for(Object o : pal){
        	LinkedTreeMap<String, Double> m = (LinkedTreeMap<String, Double>)o;
        	
    		boolean hasSwitch = false ;

    		Object switchObj = m.get("switch");
    		 
    		if(switchObj!= null && switchObj.toString().equals("true")){
    			hasSwitch = true ;
    		}
    		
        	if(m.containsKey("length"))
        	{
        		double x = m.get("length");        		
        		TrackPiece tp = new TrackPiece(i,  PieceType.STRAIGHT, x, 0,hasSwitch);
        		pieces.add(tp);
           	}
        	else
        	{
        	   double r = m.get("radius");
        	   TrackPiece tp = new TrackPiece(i,  PieceType.BEND, r, m.get("angle"), hasSwitch);
               pieces.add(tp);
                TrackKnowledge.getInstance().addNewBend(i);
        	}
        	i++;
        }

        ArrayList<Object> ls = (ArrayList<Object>) trackm.get("lanes");

        for(Object o : ls){
            Lane lane = new Lane();
            LinkedTreeMap<String, Double> l = (LinkedTreeMap<String, Double>)o;

            Double index = l.get("index") ;
            Double dist = l.get("distanceFromCenter") ;

            lane.setIndex(index.intValue());
            lane.setDistanceFromCenter(dist.doubleValue());

            getLanes().add(index.intValue(), lane);
        }
        
	}
	
	public TrackPiece getTrackPiece(int index){
		return pieces.get(index);
	}

    public boolean isTwoDirectionalCurveNext(int currentIndex){

        boolean firstCurvePositive = false;
        int next = ( currentIndex == pieces.size() - 1 ? 0 : currentIndex + 1);
        TrackPiece nextTp = null ;
        boolean fisrt = true;
        while(next != currentIndex)
        {

            TrackPiece n = getTrackPiece(next);
            if(n.getPieceType() == PieceType.STRAIGHT){
                return false;
            }

            if(fisrt){
                if(n.getAngle() > 0) firstCurvePositive = true;
            }
            else{
               if(firstCurvePositive == (n.getAngle() > 0))
                   return false;
               else
                   return true;
            }

            next = (next == pieces.size() - 1? 0 : next +1);
            boolean first = false ;
        }

        return false;

    }
    
    
    
    ///Gets the distance from one track piece to another to start of toIndex
    public double getDistance(TrackPiece from, TrackPiece to, double fromInPieceDistance, Lane lane){
    	
    	if(from == null || to == null){
    		if(from == null)
    			System.out.print("from is null");
    		else
    			System.out.print("to is null");
    		return 0;
    	}
    	
    	double distance = 0 ;
    	distance += (from.getLength(lane) - fromInPieceDistance);
    	
    	int currentIndex = from.getIndex();
    	
		int nextIndex = ( currentIndex == pieces.size() - 1 ? 0 : currentIndex + 1);
		
		while(nextIndex != to.getIndex())
		{
			distance += getTrackPiece(nextIndex).getLength(lane);
			nextIndex = ( nextIndex == pieces.size() - 1 ? 0 : nextIndex + 1);

		}

    	
    	return distance;
    }


    ///assuming that no change of lanes
	public double getDistanceToNextTrackType(int currentIndex, double currentInPieceDistance, int laneIndex){
		
		TrackPiece currentTp = getTrackPiece(currentIndex);
		double distanceToNextType = 0;
		

			distanceToNextType += (currentTp.getLength(getLane(laneIndex))-currentInPieceDistance) ;
			if(distanceToNextType < 0) {
				distanceToNextType = 0 ;
			}


		
		int next = ( currentIndex == pieces.size() - 1 ? 0 : currentIndex + 1);
		while(next != currentIndex)
		{
			TrackPiece nextTp = getTrackPiece(next);
			
			if(nextTp.getPieceType() == currentTp.getPieceType()){
				distanceToNextType += nextTp.getLength(getLane(laneIndex));
			}
			else
			{
				break;
			}
			next = (next == pieces.size() - 1? 0 : next +1);
		}
		
		if(distanceToNextType < 0) {
			distanceToNextType = 0 ;
		}
		
		return distanceToNextType;
	}

    public double getAngleOfNextBend(int currentIndex){

        TrackPiece currentTp = getTrackPiece(currentIndex);

        int next = ( currentIndex == pieces.size() - 1 ? 0 : currentIndex + 1);
        TrackPiece nextTp = null ;
        while(next != currentIndex)
        {

            TrackPiece n = getTrackPiece(next);
            if(n.getPieceType() == PieceType.BEND){
                nextTp = n;
               break;
            }

            next = (next == pieces.size() - 1? 0 : next +1);
        }

        if(nextTp == null)
            return 0;
        else
            return nextTp.getAngle();

    }

    public TrackPiece getNexBendTrackPiece(int currentIndex, double currentLap){
        
    	boolean isLastLap = (currentLap == totalLaps - 1) ; //current lap is zero based 
    	
    	//handle last lap
    	if((currentIndex == pieces.size() -1 ) && isLastLap )  
    	{
    		//it is the last lap and the last piece , there is no next bend
    		return null;
    	}
    	TrackPiece currentTp = getTrackPiece(currentIndex);

        int next = ( currentIndex == pieces.size() - 1 ? 0 : currentIndex + 1);
        TrackPiece nextTp = null ;
        while(next != currentIndex)
        {

        	if((next == pieces.size() - 1 ) && isLastLap)
            {
            	return null;
            }
        	
            TrackPiece n = getTrackPiece(next);
            if(n.getPieceType() == PieceType.BEND){
                nextTp = n;
                break;
            }
            
            next = (next == pieces.size() - 1? 0 : next +1);           
        }

        
        return nextTp;

    }
    
 

    public double getDistanceTravelled(int startIndex, int endIndex, double startInPieceDistance, double endInPieceDistance, int laneIndex){
        TrackPiece startPiece = getTrackPiece(startIndex) ;
        TrackPiece endPiece = getTrackPiece(endIndex) ;

        double distanceTravelled = 0 ;

        if(startIndex == endIndex){
            distanceTravelled += ( endInPieceDistance - startInPieceDistance ) ;
        }
        else {
            distanceTravelled += (startPiece.getLength(getLane(laneIndex)) -startInPieceDistance );

            if(distanceTravelled < 0) distanceTravelled = 0 ;

            distanceTravelled += (endInPieceDistance);

            int next = ( startIndex == pieces.size() - 1 ? 0 : startIndex + 1);

            while(next != startIndex && next != endIndex)
            {

                TrackPiece n = getTrackPiece(next);
                distanceTravelled += n.getLength(getLane(laneIndex));

                next = (next == pieces.size() - 1? 0 : next +1);
            }
        }

        return distanceTravelled;

    }

    ///Gets the next piece irrespective of the type
    public TrackPiece getNextTrackPiece(int currentIndex, double currentLap){
    	//handle last lap
    	if((currentIndex == pieces.size() -1 ) && (currentLap == totalLaps - 1))  //current lap is zero based 
    	{
    		return null;
    	}
    	
        int next = ( currentIndex == pieces.size() - 1 ? 0 : currentIndex + 1);
        return getTrackPiece(next);
    }

    ///Gets the previous piece irrespective of the type
    public TrackPiece getPreviousTrackPiece(int currentIndex){

        int prev = ( currentIndex == 0 ? pieces.size() - 1 : currentIndex - 1);
        return getTrackPiece(prev);
    }

    public ArrayList<Lane> getLanes() {
        return lanes;
    }

    public Lane getLane(int index){
        for(Lane l : lanes){
            if(l.getIndex() == index)
                return l;
        }
        
        try{
        	return lanes.get(index);
        }
        catch(Exception ex){
        	ex.printStackTrace();
        }
        
        return null;
    }

    /// 1 for right, 0 for none, -1 for left
    public int getHighestBendsTillNextSwitch(TrackPiece potentialSwitchPiece, int currentLap){
    	
    	if(potentialSwitchPiece == null)
    		return 0;
    	
    	double right = 0 ;
    	double left = 0 ;
    	
    	TrackPiece nextPiece = potentialSwitchPiece;
    	nextPiece = getNextTrackPiece(nextPiece.getIndex(), currentLap);
    	while(nextPiece != null){
    		if(nextPiece.isCanSwitch()) break;
            if(nextPiece.getType() == PieceType.BEND){
    		    if(nextPiece.getAngle() > 0 )
    			    right += nextPiece.getCenterLength();
    		    else
    		    	left += nextPiece.getCenterLength();
            }
    		
        	nextPiece = getNextTrackPiece(nextPiece.getIndex(), currentLap);
    	}
    	
    	return (right == left)? 0 : (right > left ? 1 : -1);
    }
    
    public TrackPiece GetNextSwitchPiece(int currentIndex, int currentLap){
    	
    	TrackPiece nextPiece = getNextTrackPiece(currentIndex, currentLap);
    	
    	int i = 0 ;
    	
    	while(nextPiece != null && !nextPiece.isCanSwitch()){
    		
    		//this may not be necssary , but I dont want to have an infinite loop at this stage
    		if(i > pieces.size())
    			break;
    		
    		nextPiece = getNextTrackPiece(nextPiece.getIndex(), currentLap);
    		i++;
    	}
    	
    	if(nextPiece != null && nextPiece.isCanSwitch())
    		return nextPiece;
    	else
    		return null;
    }
    
    public TrackPiece getLastTrackPiece(){
    	return getTrackPiece(pieces.size() -1);
    }


    //Return null if there is no lane in that direction
    public Lane getLaneInDir(int dir, int fromLaneIndex){
        if(dir == 0)
            return getLane(fromLaneIndex);


        if( (dir > 0  &&   lanes.size() - 1  <= fromLaneIndex)
                || (dir < 0 && fromLaneIndex <= 0)){
            return null;
        }
        else {
            int i = dir > 0 ? fromLaneIndex + 1 : fromLaneIndex -1;
            return getLane(i);
        }

    }

    public double getTotalTrackLength(){
        if(totalTrackCenterLength <= 0){
            for(TrackPiece p : pieces){
               totalTrackCenterLength +=  p.getCenterLength();
            }

            return totalTrackCenterLength;
        }
        else
            return totalTrackCenterLength;
    }

    public boolean hasBetterPositionForTurbo(CarPositions carPositions){
        double maxCheckLength = getTotalTrackLength() * TURBO_POSITION_CHECK_RATIO;
        CarPosition myCarPos = carPositions.getMyCarPosition();
        Lane myLane = getLane(myCarPos.getEndLaneIndex());

        int currentTrackIndex = myCarPos.getTrackIndex();

      //  if(currentTrackIndex == futureBestTurboIndex)
      //      return false;

        TrackPiece nextBend = getNexBendTrackPiece(currentTrackIndex,myCarPos.getLap());

        if(nextBend == null)
            return false;

        double remainingStraightDistanceNow = getDistance(getTrackPiece(myCarPos.getTrackIndex()), nextBend, myCarPos.getInPieceDistance(), myLane);

        double checkLength = remainingStraightDistanceNow;


        if(checkLength >= maxCheckLength)
            return false;

        double futureStraightLength = 0 ;
        //from next bend onwards
        TrackPiece next = nextBend ;

        int i = 0 ;


        while(next != null && i < pieces.size() ){
            i++;

            double curLen = next.getLength(myLane);

            if(next.getType() == PieceType.BEND){
                checkLength += curLen ;
                checkLength += futureStraightLength;
                futureStraightLength = 0.0 ;

            }
            else{
                if(futureBestTurboIndex == -1)
                    futureBestTurboIndex = next.getIndex();

                futureStraightLength += curLen ;
            }


            if(futureStraightLength > remainingStraightDistanceNow){
                return true;
            }



            if(checkLength > maxCheckLength){
                futureBestTurboIndex = -1;
                return false;
            }

            next = getNextTrackPiece(next.getIndex(), myCarPos.getLap());
        }

        futureBestTurboIndex = -1;
        return false;

    }

    public boolean hasSwitch(CarPosition myCarPos, double distance){

        Lane lane = getLane(myCarPos.getEndLaneIndex());
        TrackPiece next = getTrackPiece(myCarPos.getTrackIndex());

        if(next.isCanSwitch())
            return true;

        distance = distance - (next.getLength(lane) -myCarPos.getInPieceDistance() );

        int i = 0 ;
        while(distance > 0 && i < pieces.size()){
            i++;
            next = getNextTrackPiece(next.getIndex(), myCarPos.getLap());

            if(next == null)
                return false;

            if(next.isCanSwitch())
                return true;

            distance = distance - next.getLength(lane);

        }

        return false;
    }

    public int getPieceCount(){
        return this.pieces.size();
    }

}
