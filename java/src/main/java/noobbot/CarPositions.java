package noobbot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.print.attribute.standard.MediaSize.Other;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

public class CarPositions {
	
	private CarPosition myCarPosition ;
	private HashMap<String,CarPosition>  otherCarPositions = new HashMap<>();
	private int gameTick; // 0 -> race hasn't started yet
	private String gameId;
	
	
	public static CarPositions parse(String serverMsg, String myCarName){
		CarPositions cp = new CarPositions();
		
		Gson gson = new Gson();
		
		LinkedTreeMap<String , Object> json = gson.fromJson(serverMsg, LinkedTreeMap.class);
		
		if(json.get("gameTick") != null)
		 cp.gameTick = (int)(double)json.get("gameTick");
		
		cp.gameId = (String) json.get("gameId");
		ArrayList<LinkedTreeMap<String, Object>> cars = (ArrayList<LinkedTreeMap<String, Object>>) json.get("data") ;
    	for(LinkedTreeMap<String, Object> c : cars)
    	{
    		LinkedTreeMap<String, String> cid = (LinkedTreeMap<String, String>)c.get("id");
    		CarPosition currentCarPos = new CarPosition();
    		
    		LinkedTreeMap<String, Object> ppos = (LinkedTreeMap<String, Object>)c.get("piecePosition");
			Double index = Double.parseDouble(ppos.get("pieceIndex").toString());
            Double angle = Double.parseDouble(c.get("angle").toString());
            Double lap = Double.parseDouble(ppos.get("lap").toString());
			
            currentCarPos.setPieceIndex(index.intValue());
            currentCarPos.setInPieceDistance((double)ppos.get("inPieceDistance"));
            currentCarPos.setAngle(angle.doubleValue());
            currentCarPos.setLap(lap.intValue());

            //lane info
            LinkedTreeMap<String, Object> lane = (LinkedTreeMap<String, Object>)ppos.get("lane");
            Double sli = (Double)lane.get("startLaneIndex");
            Double eli = (Double)lane.get("endLaneIndex");
            currentCarPos.setStartLaneIndex(sli.intValue());
            currentCarPos.setEndLaneIndex(eli.intValue());
            
            String name = cid.get("name");
    		if(name.equals(myCarName))
    		{  // int index = 0;                
                cp.myCarPosition = currentCarPos;
    		}
    		else{
    			if(cp.getOtherCarPositions().containsKey(name)){
    				//there is a car with this name
    				//this should never happen
    				cp.getOtherCarPositions().remove(name);
    				cp.getOtherCarPositions().put(name, currentCarPos);
    			}
    			else{
    				cp.getOtherCarPositions().put(name, currentCarPos);
    			}
    		}
    		
    		
    		
    	}
    	
    	return cp;
	}
	
	
	public CarPosition getMyCarPosition() {
		return myCarPosition;
	}
	 
	public HashMap<String, CarPosition> getOtherCarPositions() {
		return otherCarPositions;
	}
	 
	public int getGameTick() {
		return gameTick;
	}
	 
	public String getGameId() {
		return gameId;
	}
	 
	
	 

}
